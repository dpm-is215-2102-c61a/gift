package com.dpmtf.gift.fragments;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.HomeCategoriesAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Category;
import com.dpmtf.gift.services.CategoryService;
import com.dpmtf.gift.services.RestService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeCategoriesFragment extends BaseFragment {

    HomeCategoriesAdapter adapter;

    RecyclerView rvHomeCategory;

    CategoryService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_categories, container, false);

        assert getActivity() != null;
        setup(); prepare();
        return view;
    }

    protected void setup() {
        rvHomeCategory = view.findViewById(R.id.rvHomeCategory);
        service = RestService.getRestService().create(CategoryService.class);
    }

    protected void prepare() {
        openPDLoader();
        list();
    }

    protected void list() {
        Call<List<Category>> call = service.list();
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                adapter = new HomeCategoriesAdapter(getContext(), response.body());
                rvHomeCategory.setAdapter(adapter);
                rvHomeCategory.setLayoutManager(new LinearLayoutManager(getContext()));
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                showMessage(getString(R.string.error));
                progressDialog.dismiss();
            }
        });
    }
}
