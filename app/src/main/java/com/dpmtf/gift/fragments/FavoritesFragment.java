package com.dpmtf.gift.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.HomeExperiencesAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.ExperienceService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.dpmtf.gift.utils.ExperienceOrigin;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritesFragment extends BaseFragment {

    HomeExperiencesAdapter adapter;

    Button btnFavoritesExperienceNoResultGoHome;
    RecyclerView rvFavoritesExperience;
    ConstraintLayout clFavoritesExperienceList, clFavoritesExperienceNoResult;

    ExperienceService service;

    List<String> favorites;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorites, container, false);
        assert getActivity() != null;
        setup(); prepare();
        return view;
    }

    protected void setup() {
        rvFavoritesExperience = view.findViewById(R.id.rvFavoritesExperience);
        clFavoritesExperienceList = view.findViewById(R.id.clFavoritesExperienceList);
        clFavoritesExperienceNoResult = view.findViewById(R.id.clFavoritesExperienceNoResult);
        btnFavoritesExperienceNoResultGoHome = view.findViewById(R.id.btnFavoritesExperienceNoResultGoHome);
        btnFavoritesExperienceNoResultGoHome.setOnClickListener(btnFavoritesExperienceNoResultGoHomeListener());

        service = RestService.getRestService().create(ExperienceService.class);
    }

    protected View.OnClickListener btnFavoritesExperienceNoResultGoHomeListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToFragment(R.id.nav_home);
            }
        };
    }

    protected void prepare() {
        openPDLoader();
        listExperienceFavorite();
    }

    protected void list() {
        User user = getCurrentUser();
        Call<List<Experience>> call = service.favorites(user.getId());
        call.enqueue(new Callback<List<Experience>>() {
            @Override
            public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
                adapter = new HomeExperiencesAdapter(getContext(), response.body(), favorites, ExperienceOrigin.FAVORITE);
                rvFavoritesExperience.setAdapter(adapter);
                rvFavoritesExperience.setLayoutManager(new LinearLayoutManager(getContext()));
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Experience>> call, Throwable t) {
                showMessage(getString(R.string.error));
                progressDialog.dismiss();
            }
        });
    }

    protected void listExperienceFavorite() {
        User user = getCurrentUser();
        Call<List<Experience>> call = service.favorites(user.getId());
        call.enqueue(new Callback<List<Experience>>() {
            @Override
            public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
                List<Experience> experiences = response.body();
                if (experiences != null) {
                    if (experiences.size() > 0) {
                        clFavoritesExperienceList.setVisibility(View.VISIBLE);
                    } else {
                        clFavoritesExperienceNoResult.setVisibility(View.VISIBLE);
                    }
                    favorites = Constants.getExperiencesIds(experiences);
                } else {
                    clFavoritesExperienceNoResult.setVisibility(View.VISIBLE);
                }
                list();
            }

            @Override
            public void onFailure(Call<List<Experience>> call, Throwable t) {
                list();
            }
        });
    }
}
