package com.dpmtf.gift.fragments.admin.category;

import android.app.AlertDialog;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;

import com.dpmtf.gift.R;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Category;
import com.dpmtf.gift.services.CategoryService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFormFragment extends BaseFragment {

    TextView tvCategoryFormTitle;

    EditText etCategoryFormName;
    ImageView ivCategoryFormImage;
    Button btnCategoryFormSave;

    Uri imageUri;

    CategoryService service;

    Category category;

    ActivityResultLauncher<String> imageActivityResult;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category_form, container, false);
        assert getActivity() != null;
        getActivity().setTitle(R.string.nav_category);
        setup();
        return view;
    }

    protected void setup() {
        tvCategoryFormTitle = view.findViewById(R.id.tvCategoryFormTitle);
        etCategoryFormName = view.findViewById(R.id.etCategoryFormName);
        ivCategoryFormImage = view.findViewById(R.id.ivCategoryFormImage);
        ivCategoryFormImage.setOnClickListener(ivCategoryFormImageListener());
        btnCategoryFormSave = view.findViewById(R.id.btnCategoryFormSave);
        btnCategoryFormSave.setOnClickListener(btnCategoryFormSaveListener());

        imageActivityResult = registerForActivityResult(new ActivityResultContracts.GetContent(), imageActivityResultCallback());

        service = RestService.getRestService().create(CategoryService.class);

        initDatabase();
        prepare();
    }

    protected View.OnClickListener ivCategoryFormImageListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageActivityResult.launch("image/*");
            }
        };
    }

    protected ActivityResultCallback<Uri> imageActivityResultCallback() {
        return new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null) {
                    imageUri = result;
                    ivCategoryFormImage.setImageURI(imageUri);
                }
            }
        };
    }

    protected View.OnClickListener btnCategoryFormSaveListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valid()) {
                    if (imageUri == null) {
                        load();
                        save();
                    } else {
                        uploadImage();
                    }
                }
            }
        };
    }

    protected boolean valid() {
        String name = etCategoryFormName.getText().toString();
        if (name.equals("")) {
            etCategoryFormName.setError(getString(R.string.category_name_label_error));
            etCategoryFormName.setFocusable(true);
            return false;
        }

        if (imageUri == null && category.getId() == null) {
            AlertDialog.Builder alertCategoryImage = new AlertDialog.Builder(getContext());
            alertCategoryImage.setMessage(getString(R.string.upload_image_alert_title));
            alertCategoryImage.setPositiveButton(getString(R.string.upload_image_alert_button_text), null);
            alertCategoryImage.create();
            alertCategoryImage.show();
            return false;
        }

        return true;
    }

    protected void create(Category category) {
        Call<Category> call = service.save(category);
        call.enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                showMessage(getString(R.string.category_form_saved));
                progressDialog.dismiss();
                goToFragment(R.id.nav_category_list);
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                showMessage(getString(R.string.error));
                progressDialog.dismiss();
            }
        });
    }

    protected void update(Category category) {
        Call<Category> call = service.update(category.getId(), category);
        call.enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                showMessage(getString(R.string.category_form_updated));
                progressDialog.dismiss();
                goToFragment(R.id.nav_category_list);
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                showMessage(getString(R.string.error));
                progressDialog.dismiss();
            }
        });
    }

    protected void save() {
        openPDLoader();
        if (category.getId() == null) {
            create(category);
        } else {
            update(category);
        }
    }

    protected void prepare() {
        category = new Category();
        category.setImage(Constants.IMAGE_DEFAULT_CATEGORY);

        CategoryFormFragmentArgs arguments = CategoryFormFragmentArgs.fromBundle(getArguments());
        String id = arguments.getId();
        if (!id.equals("")) {
            openPDLoader();

            tvCategoryFormTitle.setText(R.string.category_form_edit_title);
            btnCategoryFormSave.setText(R.string.category_form_btn_edit);

            Call<Category> call = service.get(id);
            call.enqueue(new Callback<Category>() {
                @Override
                public void onResponse(Call<Category> call, Response<Category> response) {
                    category = response.body();
                    assert category != null;
                    etCategoryFormName.setText(category.getName());
                    Picasso.get().load(category.getImage()).into(ivCategoryFormImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onError(Exception e) {
                            progressDialog.dismiss();
                        }
                    });
                }

                @Override
                public void onFailure(Call<Category> call, Throwable t) {
                    showMessage(getString(R.string.error));
                    goToFragment(R.id.nav_category_list);
                }
            });
        }
    }

    protected void load() {
        String name = etCategoryFormName.getText().toString();
        category.setName(name);
    }

    protected void uploadImage() {
        progressDialog.setMessage(getString(R.string.upload_image_loading_text));
        progressDialog.show();
        progressDialog.setCancelable(false);

        StorageReference reference = storageReference.child(Constants.DB_TABLE_CATEGORIES + "/" + UUID.randomUUID().toString() + "." + getExtension(imageUri));
        UploadTask uploadTask = reference.putFile(imageUri);
        uploadTask
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                        int progress = (100 * (int)snapshot.getBytesTransferred()) / (int)snapshot.getTotalByteCount();
                        progressDialog.setMessage(getString(R.string.upload_image_loading_text_clear) + " (" + progress + "%)");
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                        while (!uriTask.isSuccessful());
                        assert uriTask.getResult() != null;
                        String path = uriTask.getResult().toString();
                        category.setImage(path);
                        progressDialog.dismiss();
                        load();
                        save();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showMessage(getString(R.string.error));
                        progressDialog.dismiss();
                    }
                });
    }
}
