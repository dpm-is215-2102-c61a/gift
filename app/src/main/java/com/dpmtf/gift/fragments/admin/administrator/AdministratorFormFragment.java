package com.dpmtf.gift.fragments.admin.administrator;

import android.content.SharedPreferences;
import android.os.Bundle;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.dpmtf.gift.R;
import com.dpmtf.gift.activities.MainActivity;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.services.UserService;
import com.dpmtf.gift.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdministratorFormFragment extends BaseFragment {

    TextView tvAdministratorFormTitle;

    EditText etAdministratorFormName, etAdministratorFormLastname, etAdministratorFormEmail;
    Button btnAdministratorFormSave;

    UserService service;

    User administrator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_administrator_form, container, false);
        assert getActivity() != null;
        getActivity().setTitle(R.string.nav_administrator);
        initDatabase();
        setup();
        return view;
    }

    protected void setup() {
        tvAdministratorFormTitle = view.findViewById(R.id.tvAdministratorFormTitle);
        etAdministratorFormName = view.findViewById(R.id.etAdministratorFormName);
        etAdministratorFormLastname = view.findViewById(R.id.etAdministratorFormLastname);
        etAdministratorFormEmail = view.findViewById(R.id.etAdministratorFormEmail);
        btnAdministratorFormSave = view.findViewById(R.id.btnAdministratorFormSave);
        btnAdministratorFormSave.setOnClickListener(btnAdministratorFormSaveListener());

        service = RestService.getRestService().create(UserService.class);

        prepare();
    }

    protected View.OnClickListener btnAdministratorFormSaveListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                load();
                if (valid()) {
                    save();
                }
            }
        };
    }

    protected boolean valid() {
        String name = etAdministratorFormName.getText().toString();
        String lastname = etAdministratorFormLastname.getText().toString();
        String email = etAdministratorFormEmail.getText().toString();

        if (name.equals("")) {
            etAdministratorFormName.setError(getString(R.string.message_error_name));
            etAdministratorFormName.setFocusable(true);
            return false;
        }

        if (lastname.equals("")) {
            etAdministratorFormLastname.setError(getString(R.string.message_error_lastname));
            etAdministratorFormLastname.setFocusable(true);
            return false;
        }

        if (email.equals("")) {
            etAdministratorFormEmail.setError(getString(R.string.message_error_email));
            etAdministratorFormEmail.setFocusable(true);
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etAdministratorFormEmail.setError(getString(R.string.message_error_invalid_email));
            etAdministratorFormEmail.setFocusable(true);
            return false;
        }

        return true;
    }

    protected void create(User administrator) {
        Call<User> call = service.save(administrator);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                progressDialog.dismiss();
                showMessage(getString(R.string.administrator_form_saved));
                reloadUser();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                progressDialog.dismiss();
                showMessage(getString(R.string.error));
            }
        });
    }

    protected void reloadUser() {
        mAuth.signOut();
        SharedPreferences preferences = getCredentialsPreferences();
        String email = preferences.getString("email", "");
        String password = preferences.getString("password", "");
        assert getActivity() != null;
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        currentUser = mAuth.getCurrentUser();
                        goToFragment(R.id.nav_administrator_list);
                    }
                })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        goToActivityAndFinish(MainActivity.class);
                    }
                });
    }

    protected void save() {
        openPDLoader();

        assert getActivity() != null;

        mAuth.createUserWithEmailAndPassword(administrator.getEmail(), Constants.DEFAULT_PASSWORD)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            currentUser = mAuth.getCurrentUser();
                            assert currentUser != null;
                            administrator.setId(currentUser.getUid());
                            create(administrator);
                        } else {
                            showMessage(getString(R.string.message_error_register));
                            progressDialog.dismiss();
                        }
                    }
                })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showMessage(getString(R.string.message_error_register));
                        progressDialog.dismiss();
                    }
                });

        create(administrator);
    }

    protected void prepare() {
        administrator = new User();
        administrator.setImage(Constants.IMAGE_DEFAULT_PROFILE);
        administrator.setAdmin(true);
    }

    protected void load() {
        String name = etAdministratorFormName.getText().toString();
        String lastname = etAdministratorFormLastname.getText().toString();
        String email = etAdministratorFormEmail.getText().toString();

        administrator.setName(name);
        administrator.setLastname(lastname);
        administrator.setEmail(email);
    }
}
