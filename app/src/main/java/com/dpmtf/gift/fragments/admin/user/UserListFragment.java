package com.dpmtf.gift.fragments.admin.user;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.UserAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.services.UserService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserListFragment extends BaseFragment {

    TextView tvUserListTitle;
    RecyclerView rvUserList;

    UserAdapter adapter;

    UserService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_list, container, false);
        assert getActivity() != null;
        getActivity().setTitle(R.string.nav_user);
        setup(); prepare();
        return view;
    }

    protected void setup() {
        tvUserListTitle = view.findViewById(R.id.tvUserListTitle);
        rvUserList = view.findViewById(R.id.rvUserList);

        service = RestService.getRestService().create(UserService.class);
    }

    protected void prepare() {
        tvUserListTitle.setVisibility(View.INVISIBLE);
        openPDLoader();
        list();
    }

    protected void list() {
        Call<List<User>> call = service.users();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                adapter = new UserAdapter(getContext(), response.body());
                rvUserList.setAdapter(adapter);
                rvUserList.setLayoutManager(new LinearLayoutManager(getContext()));

                tvUserListTitle.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                showMessage(getString(R.string.user_list_error));
                progressDialog.dismiss();
            }
        });
    }
}
