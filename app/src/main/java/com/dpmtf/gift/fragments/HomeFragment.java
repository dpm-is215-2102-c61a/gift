package com.dpmtf.gift.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.HomeListCategoryAdapter;
import com.dpmtf.gift.adapters.HomeListExperiencePopularAdapter;
import com.dpmtf.gift.adapters.HomeListExperiencePromotionAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Category;
import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.CategoryService;
import com.dpmtf.gift.services.ExperienceService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.dpmtf.gift.utils.Experiences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment {

    HomeListCategoryAdapter homeListCategoryAdapter;
    HomeListExperiencePopularAdapter homeListExperiencePopularAdapter;
    HomeListExperiencePromotionAdapter homeListExperiencePromotionAdapter;
    RecyclerView rvHomeListCategory, rvHomeListPopular, rvHomeListPromotion;

    TextView tvHomeListCategoryViewAll, tvHomeListPopularViewAll, tvHomeListPromotionViewAll;

    CategoryService categoryService;
    ExperienceService experienceService;

    List<String> favorites;

    boolean loadCategories, loadExperiencesPopular, loadExperiencesPromotion;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        assert getActivity() != null;
        setup(); prepare();
        return view;
    }

    protected void setup() {
        loadCategories = false;
        loadExperiencesPopular = false;
        loadExperiencesPromotion = false;

        rvHomeListCategory = view.findViewById(R.id.rvHomeListCategory);
        rvHomeListPopular = view.findViewById(R.id.rvHomeListPopular);
        rvHomeListPromotion = view.findViewById(R.id.rvHomeListPromotion);

        tvHomeListCategoryViewAll = view.findViewById(R.id.tvHomeListCategoryViewAll);
        tvHomeListCategoryViewAll.setOnClickListener(tvHomeListCategoryViewAllListener());

        tvHomeListPopularViewAll = view.findViewById(R.id.tvHomeListPopularViewAll);
        tvHomeListPopularViewAll.setOnClickListener(tvHomeListPopularViewAllListener());

        tvHomeListPromotionViewAll = view.findViewById(R.id.tvHomeListPromotionViewAll);
        tvHomeListPromotionViewAll.setOnClickListener(tvHomeListPromotionViewAllListener());

        categoryService = RestService.getRestService().create(CategoryService.class);
        experienceService = RestService.getRestService().create(ExperienceService.class);

        favorites = new ArrayList<String>();
    }

    protected View.OnClickListener tvHomeListCategoryViewAllListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToFragment(R.id.nav_home_categories);
            }
        };
    }

    protected View.OnClickListener tvHomeListPopularViewAllListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeFragmentDirections.HomeExperiencesAction action = HomeFragmentDirections.homeExperiencesAction();
                action.setType(Experiences.POPULAR);
                Constants.goToFragment(view, action);
            }
        };
    }

    protected View.OnClickListener tvHomeListPromotionViewAllListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeFragmentDirections.HomeExperiencesAction action = HomeFragmentDirections.homeExperiencesAction();
                action.setType(Experiences.PROMOTION);
                Constants.goToFragment(view, action);
            }
        };
    }

    protected void prepare() {
        openPDLoader();
        listExperienceFavorite();
    }

    protected void listCategories() {
        Call<List<Category>> call = categoryService.list();
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                homeListCategoryAdapter = new HomeListCategoryAdapter(getContext(), response.body());
                rvHomeListCategory.setAdapter(homeListCategoryAdapter);
                rvHomeListCategory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

                loadCategories = true;
                hideProgressDialog();
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                showMessage(getString(R.string.error));
                loadCategories = true;
                hideProgressDialog();
            }
        });
    }

    protected void listExperiencesPopular() {
        Call<List<Experience>> call = experienceService.popular();
        call.enqueue(new Callback<List<Experience>>() {
            @Override
            public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
                homeListExperiencePopularAdapter = new HomeListExperiencePopularAdapter(getContext(), response.body(), favorites);
                rvHomeListPopular.setAdapter(homeListExperiencePopularAdapter);
                rvHomeListPopular.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

                loadExperiencesPopular = true;
                hideProgressDialog();
            }

            @Override
            public void onFailure(Call<List<Experience>> call, Throwable t) {
                showMessage(getString(R.string.error));
                loadExperiencesPopular = true;
                hideProgressDialog();
            }
        });
    }

    protected void listExperiencePromotion() {
        Call<List<Experience>> call = experienceService.promotion();
        call.enqueue(new Callback<List<Experience>>() {
            @Override
            public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
                homeListExperiencePromotionAdapter = new HomeListExperiencePromotionAdapter(getContext(), response.body(), favorites);
                rvHomeListPromotion.setAdapter(homeListExperiencePromotionAdapter);
                rvHomeListPromotion.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

                loadExperiencesPromotion = true;
                hideProgressDialog();
            }

            @Override
            public void onFailure(Call<List<Experience>> call, Throwable t) {
                showMessage(getString(R.string.error));
                loadExperiencesPromotion = true;
                hideProgressDialog();
            }
        });
    }

    protected void listExperienceFavorite() {
        User user = getCurrentUser();
        Call<List<Experience>> call = experienceService.favorites(user.getId());
        call.enqueue(new Callback<List<Experience>>() {
            @Override
            public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
                List<Experience> experiences = response.body();
                if (experiences != null) {
                    favorites = Constants.getExperiencesIds(experiences);
                }
                listCategories();
                listExperiencesPopular();
                listExperiencePromotion();
            }

            @Override
            public void onFailure(Call<List<Experience>> call, Throwable t) {
                listCategories();
                listExperiencesPopular();
                listExperiencePromotion();
            }
        });
    }

    protected void hideProgressDialog() {
        if (loadCategories && loadExperiencesPopular && loadExperiencesPromotion) {
            progressDialog.hide();
        }
    }
}
