package com.dpmtf.gift.fragments;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import android.provider.MediaStore;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.dpmtf.gift.R;
import com.dpmtf.gift.activities.LogoutActivity;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.services.UserService;
import com.dpmtf.gift.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.Objects;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment {

    FrameLayout flHomeProfileImageChange;
    CircleImageView ivHomeProfileImage;
    EditText etHomeProfileName, etHomeProfileLastname, etHomeProfileEmail, etHomeProfileCurrentPassword, etHomeProfileNewPassword, etHomeProfileConfirmPassword;

    Button btnProfileSave, btnProfileChangePassword, btnProfileChangePasswordSave, btnProfileChangePasswordCancel, btnProfileLogout;

    Uri imageUri;

    UserService service;

    ActivityResultLauncher<String> galleryActivityResult;
    ActivityResultLauncher<Intent> cameraActivityResult;

    User profile;

    private String[] cameraPermissions;

    AlertDialog changePasswordAlertDialog;

    private static final int REQUEST_CAMERA = 100;
    private static final int REQUEST_GALLERY = 200;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        setup();

        return view;
    }

    protected void setup() {
        flHomeProfileImageChange = view.findViewById(R.id.flHomeProfileImageChange);
        flHomeProfileImageChange.setOnClickListener(selectImageProfile());

        ivHomeProfileImage = view.findViewById(R.id.ivHomeProfileImage);
        etHomeProfileName = view.findViewById(R.id.etHomeProfileName);
        etHomeProfileLastname = view.findViewById(R.id.etHomeProfileLastname);
        etHomeProfileEmail = view.findViewById(R.id.etHomeProfileEmail);

        btnProfileSave = view.findViewById(R.id.btnProfileSave);
        btnProfileSave.setOnClickListener(btnProfileSaveListener());

        btnProfileChangePassword = view.findViewById(R.id.btnProfileChangePassword);
        btnProfileChangePassword.setOnClickListener(btnProfileChangePasswordListener());

        btnProfileLogout = view.findViewById(R.id.btnProfileLogout);
        btnProfileLogout.setOnClickListener(btnProfileLogoutLister());

        service = RestService.getRestService().create(UserService.class);

        cameraActivityResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), cameraActivityResultCallback());
        galleryActivityResult = registerForActivityResult(new ActivityResultContracts.GetContent(), galleryActivityResultCallback());

        cameraPermissions = new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        initDatabase();
        prepare();
    }

    protected boolean checkCameraPermissions() {
        assert getActivity() != null;
        boolean camera_permission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        boolean gallery_permission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

        return camera_permission && gallery_permission;
    }

    protected void requestCameraPermissions() {
        requestPermissions(cameraPermissions, REQUEST_CAMERA);
    }

    protected ActivityResultCallback<Uri> cameraActivityResultCallback2() {
        return new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null) {
                    imageUri = result;
                    ivHomeProfileImage.setImageURI(imageUri);
                }
            }
        };
    }

    protected ActivityResultCallback<ActivityResult> cameraActivityResultCallback() {
        return new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    //Bundle bundle = result.getData().getExtras();
                    //Bitmap bitmap = (Bitmap)bundle.get("data");
                    //ivHomeProfileImage.setImageBitmap(bitmap);
                    //imageUri = result.getData().getData();
                    ivHomeProfileImage.setImageURI(imageUri);
                }
            }
        };
    }

    protected ActivityResultCallback<Uri> galleryActivityResultCallback() {
        return new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null) {
                    imageUri = result;
                    ivHomeProfileImage.setImageURI(imageUri);
                }
            }
        };
    }

    protected View.OnClickListener selectImageProfile() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] options = {getString(R.string.profile_option_camera), getString(R.string.profile_option_gallery)};
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.profile_alert_dialog_image);
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            if (!checkCameraPermissions()) {
                                requestCameraPermissions();
                            } else {

                                ContentValues values = new ContentValues();
                                imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                                    cameraActivityResult.launch(cameraIntent);
                                }
                            }
                        } else if (i == 1) {
                            galleryActivityResult.launch("image/*");
                        }
                    }
                });
                builder.create();
                builder.show();
            }
        };
    }

    protected View.OnClickListener btnProfileSaveListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valid()) {
                    if (imageUri == null) {
                        load();
                        save();
                    } else {
                        uploadImage();
                    }
                }
            }
        };
    }

    protected View.OnClickListener btnProfileChangePasswordListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View formView = getLayoutInflater().inflate(R.layout.profile_change_password, null);
                etHomeProfileCurrentPassword = formView.findViewById(R.id.etHomeProfileCurrentPassword);
                etHomeProfileNewPassword = formView.findViewById(R.id.etHomeProfileNewPassword);
                etHomeProfileConfirmPassword = formView.findViewById(R.id.etHomeProfileConfirmPassword);

                btnProfileChangePasswordSave = formView.findViewById(R.id.btnProfileChangePasswordSave);
                btnProfileChangePasswordSave.setOnClickListener(btnProfileChangePasswordSaveListener());

                btnProfileChangePasswordCancel = formView.findViewById(R.id.btnProfileChangePasswordCancel);
                btnProfileChangePasswordCancel.setOnClickListener(btnProfileChangePasswordCancelListener());
                openChangePasswordDialog(formView);
            }
        };
    }

    protected View.OnClickListener btnProfileLogoutLister() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToActivityAndDestroy(LogoutActivity.class);
            }
        };
    }

    protected void prepare() {
        openPDLoader();
        String id = getCurrentUser().getId();
        Call<User> call = service.get(id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                profile = response.body();
                assert profile != null;
                etHomeProfileName.setText(profile.getName());
                etHomeProfileLastname.setText(profile.getLastname());
                etHomeProfileEmail.setText(profile.getEmail());
                Picasso.get().load(profile.getImage()).into(ivHomeProfileImage, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(Exception e) {
                        progressDialog.dismiss();
                    }
                });
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                showMessage(getString(R.string.error));
                goToFragment(R.id.nav_category_list);
            }
        });
    }

    protected boolean valid() {
        String name = etHomeProfileName.getText().toString();
        if (name.equals("")) {
            etHomeProfileName.setError(getString(R.string.message_error_name));
            etHomeProfileName.setFocusable(true);
            return false;
        }

        String lastname = etHomeProfileLastname.getText().toString();
        if (lastname.equals("")) {
            etHomeProfileLastname.setError(getString(R.string.message_error_lastname));
            etHomeProfileLastname.setFocusable(true);
            return false;
        }

        return true;
    }

    protected void load() {
        String name = etHomeProfileName.getText().toString();
        String lastname = etHomeProfileLastname.getText().toString();

        profile.setName(name);
        profile.setLastname(lastname);
    }

    protected void save() {
        openPDLoader();
        Call<User> call = service.update(profile.getId(), profile);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                showMessage(getString(R.string.profile_form_save));
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                showMessage(getString(R.string.error));
                progressDialog.dismiss();
            }
        });
    }

    protected void uploadImage() {
        progressDialog.setMessage(getString(R.string.upload_image_loading_text));
        progressDialog.show();
        progressDialog.setCancelable(false);

        StorageReference reference = storageReference.child(Constants.DB_TABLE_USERS + "/" + UUID.randomUUID().toString() + "." + getExtension(imageUri));
        UploadTask uploadTask = reference.putFile(imageUri);
        uploadTask
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                        int progress = (100 * (int)snapshot.getBytesTransferred()) / (int)snapshot.getTotalByteCount();
                        progressDialog.setMessage(getString(R.string.upload_image_loading_text_clear) + " (" + progress + "%)");
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                        while (!uriTask.isSuccessful());
                        assert uriTask.getResult() != null;
                        String path = uriTask.getResult().toString();
                        profile.setImage(path);
                        progressDialog.dismiss();
                        load();
                        save();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showMessage(getString(R.string.error));
                        progressDialog.dismiss();
                    }
                });
    }

    protected void openChangePasswordDialog(View formView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        builder.setView(formView);
        changePasswordAlertDialog = builder.create();
        changePasswordAlertDialog.show();
    }

    protected View.OnClickListener btnProfileChangePasswordSaveListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validChangePassword()) {
                    openPDLoader();
                    updatePassword();
                }
            }
        };
    }

    protected View.OnClickListener btnProfileChangePasswordCancelListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePasswordAlertDialog.dismiss();
            }
        };
    }

    protected boolean validChangePassword() {
        SharedPreferences preferences = getCredentialsPreferences();
        String preferencesCurrentPassword = preferences.getString("password", "");

        String currentPassword = etHomeProfileCurrentPassword.getText().toString();
        String newPassword = etHomeProfileNewPassword.getText().toString();
        String confirmPassword = etHomeProfileConfirmPassword.getText().toString();

        if (currentPassword.equals("")) {
            etHomeProfileCurrentPassword.setError(getString(R.string.message_error_password_current));
            etHomeProfileCurrentPassword.setFocusable(true);
            return false;
        }

        if (newPassword.equals("")) {
            etHomeProfileNewPassword.setError(getString(R.string.message_error_password));
            etHomeProfileNewPassword.setFocusable(true);
            return false;
        }

        if (newPassword.length() < 6) {
            etHomeProfileNewPassword.setError(getString(R.string.message_error_password_length));
            etHomeProfileNewPassword.setFocusable(true);
            return false;
        }

        if (confirmPassword.equals("")) {
            etHomeProfileConfirmPassword.setError(getString(R.string.message_error_password_confirm));
            etHomeProfileConfirmPassword.setFocusable(true);
            return false;
        }

        if (!newPassword.equals(confirmPassword)) {
            etHomeProfileConfirmPassword.setError(getString(R.string.message_error_passwords));
            etHomeProfileConfirmPassword.setFocusable(true);
            return false;
        }

        if (!currentPassword.equals(preferencesCurrentPassword)) {
            etHomeProfileCurrentPassword.setError(getString(R.string.message_error_password_current_no_match));
            etHomeProfileCurrentPassword.setFocusable(true);
            return false;
        }

        return true;
    }

    protected void updatePassword() {
        String password = etHomeProfileNewPassword.getText().toString();
        currentUser = mAuth.getCurrentUser();
        currentUser.updatePassword(password)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        User user = getCurrentUser();
                        saveUserCredentials(user.getEmail(), password);
                        showMessage(getString(R.string.profile_password_update));
                        progressDialog.dismiss();
                        changePasswordAlertDialog.dismiss();
                    }
                });
    }
}
