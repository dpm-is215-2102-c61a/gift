package com.dpmtf.gift.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.HomeExperienceDetailAdapter;
import com.dpmtf.gift.adapters.HomeExperienceFeatureAdapter;
import com.dpmtf.gift.adapters.HomeExperienceReviewAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.entities.Location;
import com.dpmtf.gift.entities.Review;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.ExperienceService;
import com.dpmtf.gift.services.FavoriteService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.services.ReviewService;
import com.dpmtf.gift.utils.Constants;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExperienceFragment extends BaseFragment {

    ImageView ivExperienceImage;
    ImageButton btnExperienceFavorite, btnExperienceContactCel, btnExperienceContactWhatsapp;
    TextView tvExperienceTitle, tvExperienceReviews, tvExperienceDescription, tvExperienceCategory, tvExperienceReviewsNoResult;
    RatingBar rbExperienceRating;
    RecyclerView rvExperienceFeatures, rvExperienceDetail, rvExperienceReviews;
    ConstraintLayout clExperienceContent, clExperienceSection27;

    HomeExperienceFeatureAdapter homeFeatureAdapter;
    HomeExperienceDetailAdapter homeExperienceDetailAdapter;
    HomeExperienceReviewAdapter homeExperienceReviewAdapter;

    List<String> favorites;

    ExperienceService experienceService;
    ReviewService reviewService;
    FavoriteService favoriteService;

    Experience experience;
    List<Review> reviews;

    GoogleMap map;
    CameraPosition cameraPosition;
    FusedLocationProviderClient fusedLocationProviderClient;
    static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    boolean locationPermissionGranted;

    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    android.location.Location lastKnownLocation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            lastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            cameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        view = inflater.inflate(R.layout.fragment_experience, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setup();
    }

    protected void setup() {
        ivExperienceImage = view.findViewById(R.id.ivExperienceImage);
        btnExperienceFavorite = view.findViewById(R.id.btnExperienceFavorite);
        btnExperienceContactCel = view.findViewById(R.id.btnExperienceContactCel);
        btnExperienceContactWhatsapp = view.findViewById(R.id.btnExperienceContactWhatsapp);
        tvExperienceTitle = view.findViewById(R.id.tvExperienceTitle);
        tvExperienceReviews = view.findViewById(R.id.tvExperienceReviews);
        tvExperienceDescription = view.findViewById(R.id.tvExperienceDescription);
        tvExperienceCategory = view.findViewById(R.id.tvExperienceCategory);
        tvExperienceReviewsNoResult = view.findViewById(R.id.tvExperienceReviewsNoResult);
        rbExperienceRating = view.findViewById(R.id.rbExperienceRating);
        rvExperienceFeatures = view.findViewById(R.id.rvExperienceFeatures);
        rvExperienceDetail = view.findViewById(R.id.rvExperienceDetail);
        rvExperienceReviews = view.findViewById(R.id.rvExperienceReviews);
        clExperienceContent = view.findViewById(R.id.clExperienceContent);
        clExperienceSection27 = view.findViewById(R.id.clExperienceSection27);

        experienceService = RestService.getRestService().create(ExperienceService.class);
        reviewService = RestService.getRestService().create(ReviewService.class);
        favoriteService = RestService.getRestService().create(FavoriteService.class);

        assert getActivity() != null;
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        prepare();
    }

    protected void prepare() {
        openPDLoader();

        ExperienceFragmentArgs arguments = ExperienceFragmentArgs.fromBundle(getArguments());
        String id = arguments.getExperienceId();
        String name = arguments.getExperienceName();

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(name);

        Call<Experience> callExperience = experienceService.get(id);
        callExperience.enqueue(new Callback<Experience>() {
            @Override
            public void onResponse(Call<Experience> call, Response<Experience> response) {
                experience = response.body();
                assert experience != null;
                Call<List<Review>> callReviews = reviewService.get(experience.getId());
                callReviews.enqueue(new Callback<List<Review>>() {
                    @Override
                    public void onResponse(Call<List<Review>> call, Response<List<Review>> response) {
                        reviews = response.body();
                        listExperienceFavorite();
                    }

                    @Override
                    public void onFailure(Call<List<Review>> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onFailure(Call<Experience> call, Throwable t) {
                showMessage(getString(R.string.error));
                goToFragment(R.id.nav_home);
            }
        });
    }

    protected void load() {
        Picasso.get().load(experience.getImage()).into(ivExperienceImage);

        clExperienceContent.setVisibility(View.VISIBLE);

        homeFeatureAdapter = new HomeExperienceFeatureAdapter(getContext(), Arrays.asList(experience.getFeatures()));
        rvExperienceFeatures.setAdapter(homeFeatureAdapter);
        rvExperienceFeatures.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));

        homeExperienceDetailAdapter = new HomeExperienceDetailAdapter(getContext(), Constants.detailtToList(experience.getDetail()));
        rvExperienceDetail.setAdapter(homeExperienceDetailAdapter);
        rvExperienceDetail.setLayoutManager(new LinearLayoutManager(getContext()));

        if (reviews.size() > 0) {
            homeExperienceReviewAdapter = new HomeExperienceReviewAdapter(getContext(), reviews);
            rvExperienceReviews.setAdapter(homeExperienceReviewAdapter);
            rvExperienceReviews.setLayoutManager(new LinearLayoutManager(getContext()));
            rvExperienceReviews.setVisibility(View.VISIBLE);
        } else {
            tvExperienceReviewsNoResult.setVisibility(View.VISIBLE);
        }

        boolean favorite = favorites.contains(experience.getId());
        if (favorite) {
            btnExperienceFavorite.setImageResource(R.drawable.ic_favorite_checked);
        }
        btnExperienceFavorite.setOnClickListener(btnExperienceFavoriteListener(btnExperienceFavorite));

        tvExperienceTitle.setText(experience.getTitle());
        tvExperienceReviews.setText(String.format(getString(R.string.experiente_rating_text), String.valueOf(experience.getRating()), experience.getReviews()));
        tvExperienceDescription.setText(experience.getDescription());
        tvExperienceCategory.setText(experience.getCategory().getName());
        rbExperienceRating.setRating(experience.getRating());

        btnExperienceContactCel.setOnClickListener(btnExperienceContactCelListener());
        btnExperienceContactWhatsapp.setOnClickListener(btnExperienceContactWhatsappListener());

        loadMap();
    }

    protected void loadMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fExperienceLocationMap);
        if (mapFragment != null) {
            mapFragment.getMapAsync(mapCallback());
        }
    }

    protected void listExperienceFavorite() {
        User user = getCurrentUser();
        Call<List<Experience>> call = experienceService.favorites(user.getId());
        call.enqueue(new Callback<List<Experience>>() {
            @Override
            public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
                List<Experience> experiences = response.body();
                if (experiences != null) {
                    favorites = Constants.getExperiencesIds(experiences);
                }
                progressDialog.dismiss();
                load();
            }

            @Override
            public void onFailure(Call<List<Experience>> call, Throwable t) {
                progressDialog.dismiss();
                load();
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (map != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, map.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, lastKnownLocation);
        }
        super.onSaveInstanceState(outState);
    }

    protected OnMapReadyCallback mapCallback() {
        return new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull GoogleMap googleMap) {
                map = googleMap;
                map.getUiSettings().setZoomControlsEnabled(true);
                getLocationPermission();
                updateLocationUI();
                getDeviceLocation();
            }
        };
    }

    protected void getDeviceLocation() {
        try {
            if (locationPermissionGranted) {
                Task<android.location.Location> locationResult = fusedLocationProviderClient.getLastLocation();
                assert getActivity() != null;
                locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener<android.location.Location>() {
                    @Override
                    public void onComplete(@NonNull Task<android.location.Location> task) {
                        if (task.isSuccessful()) {
                            lastKnownLocation = task.getResult();
                        }

                        loadMarkers();
                    }
                });
            } else {
                loadMarkers();
            }
        } catch (Exception ignored) {
            loadMarkers();
        }
    }

    protected void loadMarkers() {
        LatLng marker = null;
        Location[] locations = experience.getLocations();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        if (lastKnownLocation != null) {
            LatLng myLocation = Constants.getMarker(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
            map.addMarker(Constants.getMarkerOptions(myLocation).icon(Constants.changeIcon(getContext(), R.drawable.user_location)));
            builder.include(myLocation);
        }

        for (Location location: locations) {
            marker = Constants.getMarker(location);
            map.addMarker(Constants.getMarkerOptions(getContext(), location));
            builder.include(marker);
        }
        if (marker != null) {
            LatLngBounds bounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = view.findViewById(R.id.fExperienceLocationMap).getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (height * 0.15);

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            map.animateCamera(cameraUpdate);
        } else {
            ViewGroup parent = (ViewGroup) clExperienceSection27.getParent();
            parent.removeView(clExperienceSection27);
        }
    }

    protected View.OnClickListener btnExperienceFavoriteListener(ImageButton button) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = Constants.getCurrentUser(getContext());
                boolean favorite = favorites.contains(experience.getId());
                if (favorite) {
                    Call<Boolean> call = favoriteService.delete(experience.getId(), user.getId());
                    call.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            button.setImageResource(R.drawable.ic_favorite);
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {

                        }
                    });
                } else {
                    Call<Boolean> call = favoriteService.add(experience.getId(), user.getId());
                    call.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            button.setImageResource(R.drawable.ic_favorite_checked);
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {

                        }
                    });
                }
            }
        };
    }

    protected View.OnClickListener btnExperienceContactCelListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkCallPermissions()) {
                    requestCallPermissions();
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:012222222"));
                    startActivity(callIntent);
                }
            }
        };
    }

    protected View.OnClickListener btnExperienceContactWhatsappListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://api.whatsapp.com/send?phone=51999999999";
                Intent whatsappIntent = new Intent(Intent.ACTION_VIEW);
                whatsappIntent.setData(Uri.parse(url));
                startActivity(whatsappIntent);
            }
        };
    }

    protected boolean checkCallPermissions() {
        assert getActivity() != null;
        return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;
    }

    protected void requestCallPermissions() {
        String[] permissions = {Manifest.permission.CALL_PHONE};
        requestPermissions(permissions, 100);
    }

    private void getLocationPermission() {
        assert getActivity() != null;
        assert getContext() != null;
        if (ContextCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void updateLocationUI() {
        if (map == null) {
            return;
        }
        try {
            if (locationPermissionGranted) {
                map.setMyLocationEnabled(true);
                map.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                map.setMyLocationEnabled(false);
                map.getUiSettings().setMyLocationButtonEnabled(false);
                lastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException ignored)  {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        locationPermissionGranted = false;
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationPermissionGranted = true;
            }
        }
        updateLocationUI();
    }
}
