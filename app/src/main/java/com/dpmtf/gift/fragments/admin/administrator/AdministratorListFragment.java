package com.dpmtf.gift.fragments.admin.administrator;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.AdministratorAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.services.UserService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdministratorListFragment extends BaseFragment {

    TextView tvAdministratorListTitle;
    RecyclerView rvAdministratorList;
    FloatingActionButton btnAdministratorListNew;

    AdministratorAdapter adapter;

    UserService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_administrator_list, container, false);
        assert getActivity() != null;
        getActivity().setTitle(R.string.nav_administrator);
        setup(); prepare();
        return view;
    }

    protected void setup() {
        tvAdministratorListTitle = view.findViewById(R.id.tvAdministratorListTitle);
        rvAdministratorList = view.findViewById(R.id.rvAdministratorList);
        btnAdministratorListNew = view.findViewById(R.id.btnAdministratorListNew);
        btnAdministratorListNew.setOnClickListener(btnAdministratorListNewListener());

        service = RestService.getRestService().create(UserService.class);
    }

    protected View.OnClickListener btnAdministratorListNewListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToFragment(R.id.nav_administrator_form);
            }
        };
    }

    protected void prepare() {
        tvAdministratorListTitle.setVisibility(View.INVISIBLE);
        openPDLoader();
        list();
    }

    protected void list() {
        Call<List<User>> call = service.administrators();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                adapter = new AdministratorAdapter(getContext(), response.body());
                rvAdministratorList.setAdapter(adapter);
                rvAdministratorList.setLayoutManager(new LinearLayoutManager(getContext()));

                tvAdministratorListTitle.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                showMessage(getString(R.string.administrator_list_error));
                progressDialog.dismiss();
            }
        });
    }
}