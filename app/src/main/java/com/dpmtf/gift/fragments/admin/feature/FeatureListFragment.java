package com.dpmtf.gift.fragments.admin.feature;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.FeatureAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Feature;
import com.dpmtf.gift.services.FeatureService;
import com.dpmtf.gift.services.RestService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeatureListFragment extends BaseFragment {

    TextView tvFeatureListTitle;
    RecyclerView rvFeatureList;
    FloatingActionButton btnFeatureListNew;

    FeatureAdapter adapter;

    FeatureService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_feature_list, container, false);
        assert getActivity() != null;
        getActivity().setTitle(R.string.nav_feature);
        setup(); prepare();
        return view;
    }

    protected void setup() {
        tvFeatureListTitle = view.findViewById(R.id.tvFeatureListTitle);
        rvFeatureList = view.findViewById(R.id.rvFeatureList);
        btnFeatureListNew = view.findViewById(R.id.btnFeatureListNew);
        btnFeatureListNew.setOnClickListener(btnFeatureListNewListener());

        service = RestService.getRestService().create(FeatureService.class);
    }

    protected View.OnClickListener btnFeatureListNewListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToFragment(R.id.nav_feature_form);
            }
        };
    }

    protected void prepare() {
        tvFeatureListTitle.setVisibility(View.INVISIBLE);
        openPDLoader();
        list();
    }

    protected void list() {
        Call<List<Feature>> call = service.list();
        call.enqueue(new Callback<List<Feature>>() {
            @Override
            public void onResponse(Call<List<Feature>> call, Response<List<Feature>> response) {
                adapter = new FeatureAdapter(getContext(), response.body());
                rvFeatureList.setAdapter(adapter);
                rvFeatureList.setLayoutManager(new LinearLayoutManager(getContext()));

                tvFeatureListTitle.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Feature>> call, Throwable t) {
                showMessage(getString(R.string.feature_list_error));
                progressDialog.dismiss();
            }
        });
    }
}
