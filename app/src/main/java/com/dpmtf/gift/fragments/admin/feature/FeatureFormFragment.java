package com.dpmtf.gift.fragments.admin.feature;

import android.app.AlertDialog;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;

import com.dpmtf.gift.R;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Feature;
import com.dpmtf.gift.services.FeatureService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeatureFormFragment extends BaseFragment {

    TextView tvFeatureFormTitle;

    EditText etFeatureFormName;
    ImageView ivFeatureFormImage;
    Button btnFeatureFormSave;

    Uri imageUri;

    FeatureService service;

    Feature feature;

    ActivityResultLauncher<String> imageActivityResult;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_feature_form, container, false);
        assert getActivity() != null;
        getActivity().setTitle(R.string.nav_feature);
        setup();
        return view;
    }

    private void setup() {
        tvFeatureFormTitle = view.findViewById(R.id.tvFeatureFormTitle);
        etFeatureFormName = view.findViewById(R.id.etFeatureFormName);
        ivFeatureFormImage = view.findViewById(R.id.ivFeatureFormImage);
        ivFeatureFormImage.setOnClickListener(ivFeatureFormImageListener());
        btnFeatureFormSave = view.findViewById(R.id.btnFeatureFormSave);
        btnFeatureFormSave.setOnClickListener(btnFeatureFormSaveListener());

        imageActivityResult = registerForActivityResult(new ActivityResultContracts.GetContent(), imageActivityResultCallback());

        service = RestService.getRestService().create(FeatureService.class);

        initDatabase();
        prepare();
    }

    protected View.OnClickListener ivFeatureFormImageListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageActivityResult.launch("image/*");
            }
        };
    }

    protected ActivityResultCallback<Uri> imageActivityResultCallback() {
        return new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null) {
                    imageUri = result;
                    ivFeatureFormImage.setImageURI(imageUri);
                }
            }
        };
    }

    private View.OnClickListener btnFeatureFormSaveListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valid()) {
                    if (imageUri == null) {
                        load();
                        save();
                    } else {
                        uploadImage();
                    }
                }
            }
        };
    }

    protected boolean valid() {
        String name = etFeatureFormName.getText().toString();
        if (name.equals("")) {
            etFeatureFormName.setError(getString(R.string.feature_name_label_error));
            etFeatureFormName.setFocusable(true);
            return false;
        }

        if (imageUri == null && feature.getId() == null) {
            AlertDialog.Builder alertFeatureImage = new AlertDialog.Builder(getContext());
            alertFeatureImage.setMessage(getString(R.string.upload_image_alert_title));
            alertFeatureImage.setPositiveButton(getString(R.string.upload_image_alert_button_text), null);
            alertFeatureImage.create();
            alertFeatureImage.show();
            return false;
        }

        return true;
    }

    protected void create(Feature feature) {
        Call<Feature> call = service.save(feature);
        call.enqueue(new Callback<Feature>() {
            @Override
            public void onResponse(Call<Feature> call, Response<Feature> response) {
                showMessage(getString(R.string.feature_form_saved));
                progressDialog.dismiss();
                goToFragment(R.id.nav_feature_list);
            }

            @Override
            public void onFailure(Call<Feature> call, Throwable t) {
                showMessage(getString(R.string.error));
                progressDialog.dismiss();
            }
        });
    }

    protected void update(Feature feature) {
        Call<Feature> call = service.update(feature.getId(), feature);
        call.enqueue(new Callback<Feature>() {
            @Override
            public void onResponse(Call<Feature> call, Response<Feature> response) {
                showMessage(getString(R.string.feature_form_updated));
                progressDialog.dismiss();
                goToFragment(R.id.nav_feature_list);
            }

            @Override
            public void onFailure(Call<Feature> call, Throwable t) {
                showMessage(getString(R.string.error));
                progressDialog.dismiss();
            }
        });
    }

    protected void save() {
        openPDLoader();
        if (feature.getId() == null) {
            create(feature);
        } else {
            update(feature);
        }
    }

    protected void prepare() {
        feature = new Feature();
        feature.setImage(Constants.IMAGE_DEFAULT_FEATURE);

        FeatureFormFragmentArgs arguments = FeatureFormFragmentArgs.fromBundle(getArguments());
        String id = arguments.getId();
        if (!id.equals("")) {
            openPDLoader();

            tvFeatureFormTitle.setText(R.string.feature_form_edit_title);
            btnFeatureFormSave.setText(R.string.feature_form_btn_edit);

            Call<Feature> call = service.get(id);
            call.enqueue(new Callback<Feature>() {
                @Override
                public void onResponse(Call<Feature> call, Response<Feature> response) {
                    feature = response.body();
                    assert feature != null;
                    etFeatureFormName.setText(feature.getName());
                    Picasso.get().load(feature.getImage()).into(ivFeatureFormImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onError(Exception e) {
                            progressDialog.dismiss();
                        }
                    });
                }

                @Override
                public void onFailure(Call<Feature> call, Throwable t) {
                    showMessage(getString(R.string.error));
                    goToFragment(R.id.nav_feature_list);
                }
            });
        }
    }

    protected void load() {
        String name = etFeatureFormName.getText().toString();
        feature.setName(name);
    }

    protected void uploadImage() {
        progressDialog.setMessage(getString(R.string.upload_image_loading_text));
        progressDialog.show();
        progressDialog.setCancelable(false);

        StorageReference reference = storageReference.child(Constants.DB_TABLE_FEATURES + "/" + UUID.randomUUID().toString() + "." + getExtension(imageUri));
        UploadTask uploadTask = reference.putFile(imageUri);
        uploadTask
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                        int progress = (100 * (int)snapshot.getBytesTransferred()) / (int)snapshot.getTotalByteCount();
                        progressDialog.setMessage(getString(R.string.upload_image_loading_text_clear) + " (" + progress + "%)");
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                        while (!uriTask.isSuccessful());
                        assert uriTask.getResult() != null;
                        String path = uriTask.getResult().toString();
                        feature.setImage(path);
                        progressDialog.dismiss();
                        load();
                        save();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showMessage(getString(R.string.error));
                        progressDialog.dismiss();
                    }
                });
    }
}
