package com.dpmtf.gift.fragments.admin.experience;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.ExperienceAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.services.ExperienceService;
import com.dpmtf.gift.services.RestService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExperienceListFragment extends BaseFragment {

    TextView tvExperienceListTitle;
    RecyclerView rvExperienceList;
    FloatingActionButton btnExperienceListNew;

    ExperienceAdapter adapter;

    ExperienceService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_experience_list, container, false);
        assert getActivity() != null;
        getActivity().setTitle(R.string.nav_experience);
        setup(); prepare();
        return view;
    }

    protected void setup() {
        tvExperienceListTitle = view.findViewById(R.id.tvExperienceListTitle);
        rvExperienceList = view.findViewById(R.id.rvExperienceList);
        btnExperienceListNew = view.findViewById(R.id.btnExperienceListNew);
        btnExperienceListNew.setOnClickListener(btnExperienceListNewListener());

        service = RestService.getRestService().create(ExperienceService.class);
    }

    protected View.OnClickListener btnExperienceListNewListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToFragment(R.id.nav_experience_form);
            }
        };
    }

    protected void prepare() {
        tvExperienceListTitle.setVisibility(View.INVISIBLE);
        openPDLoader();
        list();
    }

    protected void list() {
        Call<List<Experience>> call = service.list();
        call.enqueue(new Callback<List<Experience>>() {
            @Override
            public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
                adapter = new ExperienceAdapter(getContext(), response.body());
                rvExperienceList.setAdapter(adapter);
                rvExperienceList.setLayoutManager(new LinearLayoutManager(getContext()));

                tvExperienceListTitle.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Experience>> call, Throwable t) {
                showMessage(getString(R.string.experience_list_error));
                progressDialog.dismiss();
            }
        });
    }
}
