package com.dpmtf.gift.fragments.admin.category;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.CategoryAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Category;
import com.dpmtf.gift.services.CategoryService;
import com.dpmtf.gift.services.RestService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryListFragment extends BaseFragment {

    TextView tvCategoryListTitle;
    RecyclerView rvCategoryList;
    FloatingActionButton btnCategoryListNew;

    CategoryAdapter adapter;

    CategoryService service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category_list, container, false);
        assert getActivity() != null;
        getActivity().setTitle(R.string.nav_category);
        getContext(); setup(); prepare();
        return view;
    }

    protected void setup() {
        tvCategoryListTitle = view.findViewById(R.id.tvCategoryListTitle);
        rvCategoryList = view.findViewById(R.id.rvCategoryList);
        btnCategoryListNew = view.findViewById(R.id.btnCategoryListNew);
        btnCategoryListNew.setOnClickListener(btnCategoryListNewListener());

        service = RestService.getRestService().create(CategoryService.class);
    }

    protected View.OnClickListener btnCategoryListNewListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToFragment(R.id.nav_category_form);
            }
        };
    }

    protected void prepare() {
        tvCategoryListTitle.setVisibility(View.INVISIBLE);
        openPDLoader();
        list();
    }

    public void list() {
        Call<List<Category>> call = service.list();
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                adapter = new CategoryAdapter(getContext(), response.body());
                rvCategoryList.setAdapter(adapter);
                rvCategoryList.setLayoutManager(new LinearLayoutManager(getContext()));

                tvCategoryListTitle.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                showMessage(getString(R.string.category_list_error));
                progressDialog.dismiss();
            }
        });
    }
}
