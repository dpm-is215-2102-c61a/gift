package com.dpmtf.gift.fragments;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dpmtf.gift.R;
import com.dpmtf.gift.adapters.HomeExperiencesAdapter;
import com.dpmtf.gift.classes.BaseFragment;
import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.ExperienceService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.dpmtf.gift.utils.ExperienceOrigin;
import com.dpmtf.gift.utils.Experiences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeExperiencesFragment extends BaseFragment {

    HomeExperiencesAdapter adapter;

    RecyclerView rvHomeExperience;

    ExperienceService service;

    Enum<Experiences> type;
    HomeExperiencesFragmentArgs arguments;

    List<String> favorites;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_experiences, container, false);
        assert getActivity() != null;
        setup(); prepare();
        return view;
    }

    protected void setup() {
        rvHomeExperience = view.findViewById(R.id.rvHomeExperience);
        service = RestService.getRestService().create(ExperienceService.class);

        arguments = HomeExperiencesFragmentArgs.fromBundle(getArguments());
        type = arguments.getType();
    }

    protected void prepare() {
        openPDLoader();
        if (type == Experiences.BY_CATEGORY) {
            String categoryName = arguments.getCategoryName();
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(categoryName);
        } else if (type == Experiences.POPULAR) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.home_list_popular_title);
        } else if (type == Experiences.PROMOTION) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.home_list_promotion_title);
        }
        listExperienceFavorite();
    }

    protected void list() {
        Call<List<Experience>> call = getCall();
        call.enqueue(new Callback<List<Experience>>() {
            @Override
            public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
                adapter = new HomeExperiencesAdapter(getContext(), response.body(), favorites, ExperienceOrigin.CATEGORY);
                rvHomeExperience.setAdapter(adapter);
                rvHomeExperience.setLayoutManager(new LinearLayoutManager(getContext()));
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Experience>> call, Throwable t) {
                showMessage(getString(R.string.error));
                progressDialog.dismiss();
            }
        });
    }

    protected Call<List<Experience>> getCall() {
        if (type == Experiences.BY_CATEGORY) {
            String categoryId = arguments.getCategoryId();
            return service.byCategory(categoryId);
        } else if (type == Experiences.POPULAR) {
            return service.popular();
        } else if (type == Experiences.PROMOTION) {
            return service.promotion();
        }

        return service.list();
    }

    protected void listExperienceFavorite() {
        User user = getCurrentUser();
        Call<List<Experience>> call = service.favorites(user.getId());
        call.enqueue(new Callback<List<Experience>>() {
            @Override
            public void onResponse(Call<List<Experience>> call, Response<List<Experience>> response) {
                List<Experience> experiences = response.body();
                if (experiences != null) {
                    favorites = Constants.getExperiencesIds(experiences);
                }
                list();
            }

            @Override
            public void onFailure(Call<List<Experience>> call, Throwable t) {
                list();
            }
        });
    }
}
