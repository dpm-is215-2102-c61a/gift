package com.dpmtf.gift.models;

import android.content.Context;

import androidx.annotation.NonNull;

import com.dpmtf.gift.classes.DAOBase;
import com.dpmtf.gift.entities.Category;
import com.dpmtf.gift.services.CategoryService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class DAOCategory extends DAOBase {

    private CategoryService service;

    public DAOCategory(Context context) {
        super(context);
        service = RestService.getRestService().create(CategoryService.class);
    }

    public List<Category> list() {
        List<Category> list = new ArrayList<>();
        databaseReference.child(Constants.DB_TABLE_CATEGORIES).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list.clear();
                for (DataSnapshot item:snapshot.getChildren()) {
                    Category category = item.getValue(Category.class);
                    list.add(category);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        return list;
    }

    public boolean create(Category category) {
        category.setId(UUID.randomUUID().toString());
        databaseReference.child(Constants.DB_TABLE_CATEGORIES).child(category.getId()).setValue(category);
        return true;
    }

    public boolean update(Category category) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("title", category.getName());
        databaseReference.child(Constants.DB_TABLE_CATEGORIES).child(category.getId()).updateChildren(map);
        return true;
    }

    public Category findById(String id) {
        Category category = new Category();
        databaseReference.child(Constants.DB_TABLE_CATEGORIES).child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    category.setId(snapshot.child("id").getValue().toString());
                    category.setName(snapshot.child("title").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        return category;
    }

    public boolean delete(String id) {
        databaseReference.child(Constants.DB_TABLE_CATEGORIES).child(id).removeValue();
        return true;
    }
}
