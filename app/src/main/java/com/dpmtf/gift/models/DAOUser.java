package com.dpmtf.gift.models;

import android.content.Context;

import androidx.annotation.NonNull;

import com.dpmtf.gift.classes.DAOBase;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class DAOUser extends DAOBase {

    public DAOUser(Context context) {
        super(context);
    }

    //public List<User> list() {
    //
    //}

    public boolean create(User user) {
        user.setId(UUID.randomUUID().toString());
        databaseReference.child(Constants.DB_TABLE_USERS).child(user.getId()).setValue(user);
        return true;
    }

    public boolean update(User user) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", user.getName());
        map.put("lastname", user.getLastname());
        map.put("email", user.getEmail());
        map.put("image", user.getImage());
        databaseReference.child(Constants.DB_TABLE_USERS).child(user.getId()).updateChildren(map);
        return true;
    }

    public User findById(String id) {
        User user = new User();
        databaseReference.child(Constants.DB_TABLE_USERS).child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    user.setId(snapshot.child("id").getValue().toString());
                    user.setName(snapshot.child("name").getValue().toString());
                    user.setName(snapshot.child("lastname").getValue().toString());
                    user.setName(snapshot.child("email").getValue().toString());
                    user.setName(snapshot.child("image").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        return user;
    }

    public boolean delete(String id) {
        databaseReference.child(Constants.DB_TABLE_USERS).child(id).removeValue();
        return true;
    }
}
