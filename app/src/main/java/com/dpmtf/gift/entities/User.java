package com.dpmtf.gift.entities;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("email")
    private String email;

    @SerializedName("image")
    private String image;

    @SerializedName("is_admin")
    private boolean isAdmin;

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    public User() {
    }

    public User(String name, String lastname, String email, String image, boolean isAdmin) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.image = image;
        this.isAdmin = isAdmin;
    }

    public User(String id, String name, String lastname, String email, String image, boolean isAdmin) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.image = image;
        this.isAdmin = isAdmin;
    }

    public String getFullname() {
        return getName() + " " + getLastname();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
