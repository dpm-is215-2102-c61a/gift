package com.dpmtf.gift.entities;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Experience {

    @SerializedName("id")
    private String id;

    @SerializedName("category")
    private Category category;

    @SerializedName("name")
    private String name;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("place")
    private String place;

    @SerializedName("price")
    private String price;

    @SerializedName("image")
    private String image;

    @SerializedName("detail")
    private String detail;

    @SerializedName("sold")
    private int sold;

    @SerializedName("features")
    private Feature[] features;

    @SerializedName("locations")
    private Location[] locations;

    @SerializedName("expiration")
    private String expiration;

    @SerializedName("rating")
    private float rating;

    @SerializedName("reviews")
    private int reviews;

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    public Experience() {
    }

    public Experience(String id) {
        this.id = id;
    }

    public Experience(String id, Category category, String name, String title, String description, String place, String price, String image, String detail, int sold, Feature[] features, Location[] locations, String expiration, float rating, int reviews) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.title = title;
        this.description = description;
        this.place = place;
        this.price = price;
        this.image = image;
        this.detail = detail;
        this.sold = sold;
        this.features = features;
        this.locations = locations;
        this.expiration = expiration;
        this.rating = rating;
        this.reviews = reviews;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public Feature[] getFeatures() {
        return features;
    }

    public void setFeatures(Feature[] features) {
        this.features = features;
    }

    public Location[] getLocations() {
        return locations;
    }

    public void setLocations(Location[] locations) {
        this.locations = locations;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getReviews() {
        return reviews;
    }

    public void setReviews(int reviews) {
        this.reviews = reviews;
    }
}
