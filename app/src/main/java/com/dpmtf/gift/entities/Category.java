package com.dpmtf.gift.entities;

import androidx.annotation.NonNull;

import com.dpmtf.gift.classes.BaseEntity;
import com.google.gson.annotations.SerializedName;

public class Category implements BaseEntity {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    public Category() {
    }

    public Category(String id) {
        this.id = id;
    }

    public Category(String name, String image) {
        this.name = name;
        this.image = image;
    }

    public Category(String id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
