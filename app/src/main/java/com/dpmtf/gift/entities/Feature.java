package com.dpmtf.gift.entities;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Feature {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    public Feature() {
    }

    public Feature(String id) {
        this.id = id;
    }

    public Feature(String name, String image) {
        this.name = name;
        this.image = image;
    }

    public Feature(String id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
