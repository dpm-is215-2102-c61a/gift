package com.dpmtf.gift.entities;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Review {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("user")
    private User user;

    @SerializedName("experience")
    private Experience experience;

    @SerializedName("score")
    private float score;

    @SerializedName("comment")
    private String comment;

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    public Review() {
    }

    public Review(String id) {
        this.id = id;
    }

    public Review(String name, User user, Experience experience, float score, String comment) {
        this.name = name;
        this.user = user;
        this.experience = experience;
        this.score = score;
        this.comment = comment;
    }

    public Review(String id, String name, User user, Experience experience, float score, String comment) {
        this.id = id;
        this.name = name;
        this.user = user;
        this.experience = experience;
        this.score = score;
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
