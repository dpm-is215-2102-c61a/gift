package com.dpmtf.gift.entities;

import com.google.gson.annotations.SerializedName;

public class Favorite {

    @SerializedName("id")
    private String id;

    @SerializedName("user")
    private User user;

    @SerializedName("experience")
    private Experience experience;

    public Favorite() {
    }

    public Favorite(String id) {
        this.id = id;
    }

    public Favorite(User user, Experience experience) {
        this.user = user;
        this.experience = experience;
    }

    public Favorite(String id, User user, Experience experience) {
        this.id = id;
        this.user = user;
        this.experience = experience;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }
}
