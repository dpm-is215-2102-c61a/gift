package com.dpmtf.gift.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.activities.HomeAdminActivity;
import com.dpmtf.gift.entities.Feature;
import com.dpmtf.gift.fragments.admin.feature.FeatureListFragmentDirections;
import com.dpmtf.gift.services.FeatureService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeatureAdapter extends RecyclerView.Adapter<FeatureAdapter.FeatureViewHolder> {

    private final Context context;
    private final List<Feature> features;

    FeatureService service;

    public FeatureAdapter(Context context, List<Feature> features) {
        this.context = context;
        this.features = features;
        service = RestService.getRestService().create(FeatureService.class);
    }

    @NonNull
    @Override
    public FeatureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.feature_list_row, parent, false);
        return new FeatureViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeatureAdapter.FeatureViewHolder holder, int position) {
        Feature feature = features.get(position);

        Picasso.get().load(feature.getImage()).into(holder.ivFeatureListImage);
        holder.tvFeatureListName.setText(feature.getName());
        holder.btnFeatureListEdit.setOnClickListener(btnFeatureListEditLister(feature));
        holder.btnFeatureListDelete.setOnClickListener(btnFeatureListDelete(feature, holder));
    }

    protected View.OnClickListener btnFeatureListEditLister(Feature feature) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FeatureListFragmentDirections.FeatureAction action = FeatureListFragmentDirections.featureAction();
                action.setId(feature.getId());
                Navigation.findNavController(view).navigate(action);
            }
        };
    }

    protected View.OnClickListener btnFeatureListDelete(Feature feature, FeatureAdapter.FeatureViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertFeatureDelete = new AlertDialog.Builder(context);
                alertFeatureDelete.setMessage(context.getString(R.string.feature_list_delete_alert) + "\n\n" + feature.getName());
                alertFeatureDelete.setPositiveButton(R.string.alert_yes_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Call<Boolean> call = service.delete(feature.getId());
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                features.remove(holder.getAdapterPosition());
                                notifyItemRemoved(holder.getAdapterPosition());
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.feature_form_deleted));
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.error));
                            }
                        });
                    }
                });
                alertFeatureDelete.setNegativeButton(R.string.alert_no, null);
                alertFeatureDelete.create();
                alertFeatureDelete.show();
            }
        };
    }

    @Override
    public int getItemCount() {
        return features.size();
    }

    public class FeatureViewHolder extends RecyclerView.ViewHolder {
        TextView tvFeatureListName;
        ImageView ivFeatureListImage;
        ImageButton btnFeatureListDelete, btnFeatureListEdit;

        public FeatureViewHolder(@NonNull View itemView) {
            super(itemView);
            tvFeatureListName = itemView.findViewById(R.id.tvFeatureListName);
            ivFeatureListImage = itemView.findViewById(R.id.ivFeatureListImage);
            btnFeatureListDelete = itemView.findViewById(R.id.btnFeatureListDelete);
            btnFeatureListEdit = itemView.findViewById(R.id.btnFeatureListEdit);
        }
    }
}
