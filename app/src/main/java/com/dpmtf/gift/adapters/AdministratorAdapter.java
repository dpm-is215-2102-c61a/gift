package com.dpmtf.gift.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.activities.HomeAdminActivity;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.services.UserService;
import com.dpmtf.gift.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdministratorAdapter extends RecyclerView.Adapter<AdministratorAdapter.AdministratorViewHolder> {

    FirebaseAuth firebaseAuth;
    FirebaseUser currentUser;

    private final Context context;
    private final List<User> administrators;

    UserService service;

    public AdministratorAdapter(Context context, List<User> administrators) {
        this.context = context;
        this.administrators = administrators;
        service = RestService.getRestService().create(UserService.class);
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
    }

    @NonNull
    @Override
    public AdministratorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.administrator_list_row, parent, false);
        return new AdministratorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdministratorAdapter.AdministratorViewHolder holder, int position) {
        User administrator = administrators.get(position);

        Picasso.get().load(administrator.getImage()).into(holder.ivAdministratorListImage);
        holder.tvAdministratorListName.setText(administrator.getFullname());
        holder.tvAdministratorListEmail.setText(administrator.getEmail());

        if (administratorHasSession(administrator)) {
            holder.btnAdministratorListUnlock.setVisibility(View.INVISIBLE);
            holder.btnAdministratorListDelete.setVisibility(View.INVISIBLE);
        } else {
            holder.btnAdministratorListUnlock.setOnClickListener(btnAdministratorListUnlockListener(administrator, holder));
            holder.btnAdministratorListDelete.setOnClickListener(btnAdministratorListDeleteListener(administrator, holder));
        }
    }

    protected View.OnClickListener btnAdministratorListUnlockListener(User administrator, AdministratorAdapter.AdministratorViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertAdministratorUnlock = new AlertDialog.Builder(context);
                alertAdministratorUnlock.setMessage(context.getString(R.string.administrator_list_unlock_alert) + "\n\n" + administrator.getFullname());
                alertAdministratorUnlock.setPositiveButton(R.string.alert_yes_unlock, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        administrator.setAdmin(false);
                        Call<User> call = service.update(administrator.getId(), administrator);
                        call.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                administrators.remove(holder.getAdapterPosition());
                                notifyItemRemoved(holder.getAdapterPosition());
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.administrator_form_unlocked));
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.error));
                            }
                        });
                    }
                });
                alertAdministratorUnlock.setNegativeButton(R.string.alert_no, null);
                alertAdministratorUnlock.create();
                alertAdministratorUnlock.show();
            }
        };
    }

    protected View.OnClickListener btnAdministratorListDeleteListener(User administrator, AdministratorViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertAdministratorUnlock = new AlertDialog.Builder(context);
                alertAdministratorUnlock.setMessage(context.getString(R.string.administrator_list_delete_alert) + "\n\n" + administrator.getFullname());
                alertAdministratorUnlock.setPositiveButton(R.string.alert_yes_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Call<Boolean> call = service.delete(administrator.getId());
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                administrators.remove(holder.getAdapterPosition());
                                notifyItemRemoved(holder.getAdapterPosition());
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.administrator_form_deleted));
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.error));
                            }
                        });
                    }
                });
                alertAdministratorUnlock.setNegativeButton(R.string.alert_no, null);
                alertAdministratorUnlock.create();
                alertAdministratorUnlock.show();
            }
        };
    }

    @Override
    public int getItemCount() {
        return administrators.size();
    }

    public class AdministratorViewHolder extends RecyclerView.ViewHolder {
        ImageView ivAdministratorListImage;
        TextView tvAdministratorListName, tvAdministratorListEmail;
        ImageButton btnAdministratorListUnlock, btnAdministratorListDelete;

        public AdministratorViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAdministratorListImage = itemView.findViewById(R.id.ivAdministratorListImage);
            tvAdministratorListName = itemView.findViewById(R.id.tvAdministratorListName);
            tvAdministratorListEmail = itemView.findViewById(R.id.tvAdministratorListEmail);
            btnAdministratorListUnlock = itemView.findViewById(R.id.btnAdministratorListUnlock);
            btnAdministratorListDelete = itemView.findViewById(R.id.btnAdministratorListDelete);
        }
    }

    protected boolean administratorHasSession(User administrator) {
        return administrator.getId().equals(currentUser.getUid());
    }
}
