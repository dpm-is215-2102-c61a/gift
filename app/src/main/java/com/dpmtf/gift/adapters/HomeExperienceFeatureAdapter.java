package com.dpmtf.gift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.entities.Feature;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeExperienceFeatureAdapter extends RecyclerView.Adapter<HomeExperienceFeatureAdapter.HomeFeatureViewHolder> {

    private final Context context;
    private final List<Feature> features;

    public HomeExperienceFeatureAdapter(Context context, List<Feature> features) {
        this.context = context;
        this.features = features;
    }

    @NonNull
    @Override
    public HomeFeatureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.experience_feature_list_row, parent, false);
        return new HomeFeatureViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeFeatureViewHolder holder, int position) {
        Feature feature = features.get(position);

        Picasso.get().load(feature.getImage()).into(holder.ivExperienceFeatureImage);
        holder.tvExperienceFeatureTitle.setText(feature.getName());
    }

    @Override
    public int getItemCount() {
        return features.size();
    }

    public class HomeFeatureViewHolder extends RecyclerView.ViewHolder {

        ImageView ivExperienceFeatureImage;
        TextView tvExperienceFeatureTitle;

        public HomeFeatureViewHolder(@NonNull View itemView) {
            super(itemView);
            ivExperienceFeatureImage = itemView.findViewById(R.id.ivExperienceFeatureImage);
            tvExperienceFeatureTitle = itemView.findViewById(R.id.tvExperienceFeatureTitle);
        }
    }
}
