package com.dpmtf.gift.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.activities.HomeAdminActivity;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.services.UserService;
import com.dpmtf.gift.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private final Context context;
    private final List<User> users;

    UserService service;

    public UserAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
        service = RestService.getRestService().create(UserService.class);
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.user_list_row, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.UserViewHolder holder, int position) {
        User user = users.get(position);

        Picasso.get().load(user.getImage()).into(holder.ivUserListImage);
        holder.tvUserListName.setText(user.getFullname());
        holder.tvUserListEmail.setText(user.getEmail());
        holder.btnAdministratorListLock.setOnClickListener(btnUserListLockListener(user, holder));
    }

    protected View.OnClickListener btnUserListLockListener(User user, UserAdapter.UserViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertUserLock = new AlertDialog.Builder(context);
                alertUserLock.setMessage(context.getString(R.string.user_list_lock_alert) + "\n\n" + user.getFullname());
                alertUserLock.setPositiveButton(R.string.alert_yes_lock, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        user.setAdmin(true);
                        Call<User> call = service.update(user.getId(), user);
                        call.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                users.remove(holder.getAdapterPosition());
                                notifyItemRemoved(holder.getAdapterPosition());
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.user_form_locked));
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.error));
                            }
                        });
                    }
                });
                alertUserLock.setNegativeButton(R.string.alert_no, null);
                alertUserLock.create();
                alertUserLock.show();
            }
        };
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        ImageView ivUserListImage;
        TextView tvUserListName, tvUserListEmail;
        ImageButton btnAdministratorListLock;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            ivUserListImage = itemView.findViewById(R.id.ivUserListImage);
            tvUserListName = itemView.findViewById(R.id.tvUserListName);
            tvUserListEmail = itemView.findViewById(R.id. tvUserListEmail);
            btnAdministratorListLock = itemView.findViewById(R.id.btnAdministratorListLock);
        }
    }
}
