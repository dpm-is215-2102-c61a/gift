package com.dpmtf.gift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.entities.Category;
import com.dpmtf.gift.fragments.HomeCategoriesFragmentDirections;
import com.dpmtf.gift.fragments.HomeFragmentDirections;
import com.dpmtf.gift.utils.Constants;
import com.dpmtf.gift.utils.Experiences;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeListCategoryAdapter extends RecyclerView.Adapter<HomeListCategoryAdapter.HomeListCategoryViewHolder> {

    private final Context context;
    private final List<Category> categories;

    public HomeListCategoryAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public HomeListCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.home_section_category_list_row, parent, false);
        return new HomeListCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeListCategoryViewHolder holder, int position) {
        Category category = categories.get(position);

        Picasso.get().load(category.getImage()).into(holder.ivHomeListCategoryImage);
        holder.tvHomeListCategoryName.setText(category.getName());
        holder.clHomeListCategoryItem.setOnClickListener(clHomeListCategoryItemListener(category));
    }

    protected View.OnClickListener clHomeListCategoryItemListener(Category category) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeFragmentDirections.HomeExperiencesAction action = HomeFragmentDirections.homeExperiencesAction();
                action.setType(Experiences.BY_CATEGORY);
                action.setCategoryId(category.getId());
                action.setCategoryName(category.getName());
                Constants.goToFragment(view, action);
            }
        };
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class HomeListCategoryViewHolder extends RecyclerView.ViewHolder {

        TextView tvHomeListCategoryName;
        ImageView ivHomeListCategoryImage;
        ConstraintLayout clHomeListCategoryItem;

        public HomeListCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvHomeListCategoryName = itemView.findViewById(R.id.tvHomeListCategoryName);
            ivHomeListCategoryImage = itemView.findViewById(R.id.ivHomeListCategoryImage);
            clHomeListCategoryItem = itemView.findViewById(R.id.clHomeListCategoryItem);
        }
    }
}
