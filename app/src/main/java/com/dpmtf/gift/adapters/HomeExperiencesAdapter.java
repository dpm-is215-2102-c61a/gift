package com.dpmtf.gift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.fragments.FavoritesFragmentDirections;
import com.dpmtf.gift.fragments.HomeExperiencesFragmentDirections;
import com.dpmtf.gift.services.FavoriteService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.dpmtf.gift.utils.ExperienceOrigin;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeExperiencesAdapter extends RecyclerView.Adapter<HomeExperiencesAdapter.HomeExperiencesViewHolder> {

    private final Context context;
    private final List<Experience> experiences;
    private final List<String> favorites;
    private ExperienceOrigin from;

    FavoriteService service;

    public HomeExperiencesAdapter(Context context, List<Experience> experiences, List<String> favorites, ExperienceOrigin from) {
        this.context = context;
        this.experiences = experiences;
        this.favorites = favorites;
        this.from = from;
        service = RestService.getRestService().create(FavoriteService.class);
    }

    @NonNull
    @Override
    public HomeExperiencesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.home_experience_list_row, parent, false);
        return new HomeExperiencesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeExperiencesViewHolder holder, int position) {
        Experience experience = experiences.get(position);

        boolean favorite = false;

        if (favorites.contains(experience.getId())) {
            holder.btnHomeExperienceFavorite.setImageResource(R.drawable.ic_favorite_checked);
            favorite = true;
        }

        Picasso.get().load(experience.getImage()).into(holder.ivHomeExperienceImage);
        holder.tvHomeExperienceTitle.setText(experience.getName());
        holder.tvHomeExperiencePlace.setText(experience.getPlace());
        holder.tvHomeExperiencePrice.setText("S/. " + experience.getPrice());
        holder.tvHomeExperienceSold.setText(experience.getSold() + " vendidos");
        holder.tvHomeExperienceCategory.setText(experience.getCategory().getName());
        holder.rbHomeExperienceRating.setRating(experience.getRating());

        holder.btnHomeExperienceFavorite.setOnClickListener(btnHomeExperienceFavoriteListener(holder.btnHomeExperienceFavorite, experience, favorite));
        holder.clHomeExperienceItem.setOnClickListener(clHomeExperienceItemListener(experience));
    }

    protected View.OnClickListener clHomeExperienceItemListener(Experience experience) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (from == ExperienceOrigin.CATEGORY) {
                    HomeExperiencesFragmentDirections.CategoryExperiencesAction action = HomeExperiencesFragmentDirections.categoryExperiencesAction();
                    action.setExperienceId(experience.getId());
                    action.setExperienceName(experience.getName());
                    Navigation.findNavController(view).navigate(action);
                } else if (from == ExperienceOrigin.FAVORITE) {
                    FavoritesFragmentDirections.FavoriteExperiencesAction action = FavoritesFragmentDirections.favoriteExperiencesAction();
                    action.setExperienceId(experience.getId());
                    action.setExperienceName(experience.getName());
                    Navigation.findNavController(view).navigate(action);
                }
            }
        };
    }

    protected View.OnClickListener btnHomeExperienceFavoriteListener(ImageButton button, Experience experience, boolean favorite) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = Constants.getCurrentUser(context);
                if (favorite) {
                    Call<Boolean> call = service.delete(experience.getId(), user.getId());
                    call.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            button.setImageResource(R.drawable.ic_favorite);
                            button.setOnClickListener(btnHomeExperienceFavoriteListener(button, experience, false));
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {

                        }
                    });
                } else {
                    Call<Boolean> call = service.add(experience.getId(), user.getId());
                    call.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            button.setImageResource(R.drawable.ic_favorite_checked);
                            button.setOnClickListener(btnHomeExperienceFavoriteListener(button, experience, true));
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {

                        }
                    });
                }

            }
        };
    }

    @Override
    public int getItemCount() {
        return experiences == null ? 0 : experiences.size();
    }

    public class HomeExperiencesViewHolder extends RecyclerView.ViewHolder {

        TextView tvHomeExperienceTitle, tvHomeExperiencePlace, tvHomeExperiencePrice, tvHomeExperienceSold, tvHomeExperienceCategory;
        RatingBar rbHomeExperienceRating;
        ImageView ivHomeExperienceImage;
        ImageButton btnHomeExperienceFavorite;

        ConstraintLayout clHomeExperienceItem;

        public HomeExperiencesViewHolder(@NonNull View itemView) {
            super(itemView);
            tvHomeExperienceTitle = itemView.findViewById(R.id.tvHomeExperienceTitle);
            tvHomeExperiencePlace = itemView.findViewById(R.id.tvHomeExperiencePlace);
            tvHomeExperiencePrice = itemView.findViewById(R.id.tvHomeExperiencePrice);
            tvHomeExperienceSold = itemView.findViewById(R.id.tvHomeExperienceSold);
            tvHomeExperienceCategory = itemView.findViewById(R.id.tvHomeExperienceCategory);
            rbHomeExperienceRating = itemView.findViewById(R.id.rbHomeExperienceRating);
            ivHomeExperienceImage = itemView.findViewById(R.id.ivHomeExperienceImage);
            btnHomeExperienceFavorite = itemView.findViewById(R.id.btnHomeExperienceFavorite);
            clHomeExperienceItem = itemView.findViewById(R.id.clHomeExperienceItem);
        }
    }
}
