package com.dpmtf.gift.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.activities.HomeAdminActivity;
import com.dpmtf.gift.entities.Category;
import com.dpmtf.gift.fragments.admin.category.CategoryListFragmentDirections;
import com.dpmtf.gift.services.CategoryService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private final Context context;
    private final List<Category> categories;

    CategoryService service;

    public CategoryAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.categories = categories;
        service = RestService.getRestService().create(CategoryService.class);
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.category_list_row, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.CategoryViewHolder holder, int position) {
        Category category = categories.get(position);

        Picasso.get().load(category.getImage()).into(holder.ivCategoryListImage);
        holder.tvCategoryListName.setText(category.getName());
        holder.btnCategoryListEdit.setOnClickListener(btnCategoryListEditListener(category));
        holder.btnCategoryListDelete.setOnClickListener(btnCategoryListDeleteListener(category, holder));
    }

    protected View.OnClickListener btnCategoryListEditListener(Category category) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CategoryListFragmentDirections.CategoryAction action = CategoryListFragmentDirections.categoryAction();
                action.setId(category.getId());
                Navigation.findNavController(view).navigate(action);
            }
        };
    }

    protected View.OnClickListener btnCategoryListDeleteListener(Category category, CategoryAdapter.CategoryViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertCategoryDelete = new AlertDialog.Builder(context);
                alertCategoryDelete.setMessage(context.getString(R.string.category_list_delete_alert) + "\n\n" + category.getName());
                alertCategoryDelete.setPositiveButton(R.string.alert_yes_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Call<Boolean> call = service.delete(category.getId());
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                categories.remove(holder.getAdapterPosition());
                                notifyItemRemoved(holder.getAdapterPosition());
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.category_form_deleted));
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {
                                Constants.showMessage((HomeAdminActivity)context, context.getString(R.string.error));
                            }
                        });
                    }
                });
                alertCategoryDelete.setNegativeButton(R.string.alert_no, null);
                alertCategoryDelete.create();
                alertCategoryDelete.show();
            }
        };
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategoryListName;
        ImageView ivCategoryListImage;
        ImageButton btnCategoryListDelete, btnCategoryListEdit;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoryListName = itemView.findViewById(R.id.tvCategoryListName);
            ivCategoryListImage = itemView.findViewById(R.id.ivCategoryListImage);
            btnCategoryListEdit = itemView.findViewById(R.id.btnCategoryListEdit);
            btnCategoryListDelete = itemView.findViewById(R.id.btnCategoryListDelete);
        }
    }
}
