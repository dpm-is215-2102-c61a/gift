package com.dpmtf.gift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.fragments.HomeFragmentDirections;
import com.dpmtf.gift.services.FavoriteService;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.utils.Constants;
import com.dpmtf.gift.utils.Experiences;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeListExperiencePopularAdapter extends RecyclerView.Adapter<HomeListExperiencePopularAdapter.HomeListExperiencePopularViewHolder> {

    private final Context context;
    private final List<Experience> experiences;
    private final List<String> favorites;

    FavoriteService service;

    public HomeListExperiencePopularAdapter(Context context, List<Experience> experiences, List<String> favorites) {
        this.context = context;
        this.experiences = experiences;
        this.favorites = favorites;
        service = RestService.getRestService().create(FavoriteService.class);
    }

    @NonNull
    @Override
    public HomeListExperiencePopularViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.home_section_experience_list_row, parent, false);
        return new HomeListExperiencePopularViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeListExperiencePopularViewHolder holder, int position) {
        Experience experience = experiences.get(position);

        boolean favorite = false;

        if (favorites.contains(experience.getId())) {
            holder.btnHomeListExperiencePopularFavorite.setImageResource(R.drawable.ic_favorite_checked);
            favorite = true;
        }

        Picasso.get().load(experience.getImage()).into(holder.ivHomeListExperiencePopularImage);
        holder.btnHomeListExperiencePopularFavorite.setOnClickListener(btnHomeListExperiencePopularFavoriteListener(experience));
        holder.clHomeListExperiencePopular.setOnClickListener(clHomeListExperiencePopularListener(experience));

        holder.btnHomeListExperiencePopularFavorite.setOnClickListener(btnHomeListExperiencePopularFavoriteListener(holder.btnHomeListExperiencePopularFavorite, experience, favorite));
    }

    protected View.OnClickListener btnHomeListExperiencePopularFavoriteListener(ImageButton button, Experience experience, boolean favorite) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = Constants.getCurrentUser(context);
                if (favorite) {
                    Call<Boolean> call = service.delete(experience.getId(), user.getId());
                    call.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            button.setImageResource(R.drawable.ic_favorite);
                            button.setOnClickListener(btnHomeListExperiencePopularFavoriteListener(button, experience, false));
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {

                        }
                    });
                } else {
                    Call<Boolean> call = service.add(experience.getId(), user.getId());
                    call.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            button.setImageResource(R.drawable.ic_favorite_checked);
                            button.setOnClickListener(btnHomeListExperiencePopularFavoriteListener(button, experience, true));
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {

                        }
                    });
                }
            }
        };
    }

    protected View.OnClickListener btnHomeListExperiencePopularFavoriteListener(Experience experience) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "btn" + experience.getTitle(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    protected View.OnClickListener clHomeListExperiencePopularListener(Experience experience) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeFragmentDirections.HomeExperienceAction action = HomeFragmentDirections.homeExperienceAction();
                action.setExperienceId(experience.getId());
                action.setExperienceName(experience.getName());
                Constants.goToFragment(view, action);
            }
        };
    }

    @Override
    public int getItemCount() {
        return experiences.size();
    }

    public class HomeListExperiencePopularViewHolder extends RecyclerView.ViewHolder {

        ImageView ivHomeListExperiencePopularImage;
        ImageButton btnHomeListExperiencePopularFavorite;
        ConstraintLayout clHomeListExperiencePopular;

        public HomeListExperiencePopularViewHolder(@NonNull View itemView) {
            super(itemView);
            ivHomeListExperiencePopularImage = itemView.findViewById(R.id.ivHomeListExperienceImage);
            btnHomeListExperiencePopularFavorite = itemView.findViewById(R.id.btnHomeListExperienceFavorite);
            clHomeListExperiencePopular = itemView.findViewById(R.id.clHomeListExperience);
        }
    }
}
