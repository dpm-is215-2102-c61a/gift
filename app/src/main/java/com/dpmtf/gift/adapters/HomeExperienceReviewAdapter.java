package com.dpmtf.gift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.entities.Review;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeExperienceReviewAdapter extends RecyclerView.Adapter<HomeExperienceReviewAdapter.HomeExperienceReviewViewHolder> {

    private final Context context;
    private final List<Review> reviews;

    public HomeExperienceReviewAdapter(Context context, List<Review> reviews) {
        this.context = context;
        this.reviews = reviews;
    }

    @NonNull
    @Override
    public HomeExperienceReviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.experience_review_list_row, parent, false);
        return new HomeExperienceReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeExperienceReviewViewHolder holder, int position) {
        Review review = reviews.get(position);

        Picasso.get().load(review.getUser().getImage()).into(holder.ivExperienceReviewUserImage);
        holder.tvExperienceReviewUserName.setText(review.getUser().getFullname());
        holder.tvExperienceReviewComment.setText(review.getComment());
        holder.rbExperienceReviewRating.setRating(review.getScore());
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public class HomeExperienceReviewViewHolder extends RecyclerView.ViewHolder {

        ImageView ivExperienceReviewUserImage;
        TextView tvExperienceReviewUserName, tvExperienceReviewComment;
        RatingBar rbExperienceReviewRating;

        public HomeExperienceReviewViewHolder(@NonNull View itemView) {
            super(itemView);
            ivExperienceReviewUserImage = itemView.findViewById(R.id.ivExperienceReviewUserImage);
            tvExperienceReviewUserName = itemView.findViewById(R.id.tvExperienceReviewUserName);
            tvExperienceReviewComment = itemView.findViewById(R.id.tvExperienceReviewComment);
            rbExperienceReviewRating = itemView.findViewById(R.id.rbExperienceReviewRating);
        }
    }
}
