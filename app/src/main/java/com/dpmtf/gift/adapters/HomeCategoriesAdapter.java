package com.dpmtf.gift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.entities.Category;
import com.dpmtf.gift.fragments.HomeCategoriesFragmentDirections;
import com.dpmtf.gift.utils.Constants;
import com.dpmtf.gift.utils.Experiences;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeCategoriesAdapter extends RecyclerView.Adapter<HomeCategoriesAdapter.HomeCategoriesViewHolder> {

    private final Context context;
    private final List<Category> categories;

    public HomeCategoriesAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public HomeCategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.home_category_list_row, parent, false);
        return new HomeCategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeCategoriesViewHolder holder, int position) {
        Category category = categories.get(position);

        Picasso.get().load(category.getImage()).into(holder.ivHomeCategoryImage);
        holder.tvHomeCategoryName.setText(category.getName());
        holder.clHomeCategoryItem.setOnClickListener(clHomeCategoryItemListener(category));
    }

    protected View.OnClickListener clHomeCategoryItemListener(Category category) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeCategoriesFragmentDirections.HomeCategoriesExperiencesAction action = HomeCategoriesFragmentDirections.homeCategoriesExperiencesAction();
                action.setType(Experiences.BY_CATEGORY);
                action.setCategoryId(category.getId());
                action.setCategoryName(category.getName());
                Constants.goToFragment(view, action);
            }
        };
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class HomeCategoriesViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout clHomeCategoryItem;
        ImageView ivHomeCategoryImage;
        TextView tvHomeCategoryName;

        public HomeCategoriesViewHolder(@NonNull View itemView) {
            super(itemView);
            clHomeCategoryItem = itemView.findViewById(R.id.clHomeCategoryItem);
            ivHomeCategoryImage = itemView.findViewById(R.id.ivHomeCategoryImage);
            tvHomeCategoryName = itemView.findViewById(R.id.tvHomeCategoryName);
        }
    }
}
