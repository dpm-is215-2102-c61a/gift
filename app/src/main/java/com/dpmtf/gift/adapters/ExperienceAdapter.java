package com.dpmtf.gift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.services.ExperienceService;
import com.dpmtf.gift.services.RestService;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ExperienceAdapter extends RecyclerView.Adapter<ExperienceAdapter.ExperienceViewHolder> {

    private final Context context;
    private final List<Experience> experiences;

    ExperienceService service;

    public ExperienceAdapter(Context context, List<Experience> experiences) {
        this.context = context;
        this.experiences = experiences;
        service = RestService.getRestService().create(ExperienceService.class);
    }

    @NonNull
    @Override
    public ExperienceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.experience_list_row, parent, false);
        return new ExperienceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExperienceAdapter.ExperienceViewHolder holder, int position) {
        Experience experience = experiences.get(holder.getAdapterPosition());

        Picasso.get().load(experience.getImage()).into(holder.ivExperienceListImage);

        holder.tvExperienceListName.setText(experience.getName());
        holder.tvExperienceListPlace.setText(experience.getPlace());
        holder.tvExperienceListPrice.setText(String.format("S/.%s", experience.getPrice()));
        holder.rbExperienceListRating.setRating(experience.getRating());
        holder.tvExperienceListReviews.setText(String.valueOf(experience.getReviews()));
        //holder.btnExperienceListEdit.setOnClickListener(btnExperienceListEditListener());
        //holder.btnExperienceListDelete.setOnClickListener(btnExperienceListDeleteListener());
    }

    protected View.OnClickListener btnExperienceListEditListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };
    }

    protected View.OnClickListener btnExperienceListDeleteListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };
    }

    @Override
    public int getItemCount() {
        return experiences.size();
    }

    public class ExperienceViewHolder extends RecyclerView.ViewHolder {
        ImageView ivExperienceListImage;
        TextView tvExperienceListName, tvExperienceListPlace, tvExperienceListPrice;
        RatingBar rbExperienceListRating;
        TextView tvExperienceListReviews;
        //Button btnExperienceListEdit, btnExperienceListDelete;

        public ExperienceViewHolder(@NonNull View itemView) {
            super(itemView);
            ivExperienceListImage = itemView.findViewById(R.id.ivExperienceListImage);
            tvExperienceListName = itemView.findViewById(R.id.tvExperienceListName);
            tvExperienceListPlace = itemView.findViewById(R.id.tvExperienceListPlace);
            tvExperienceListPrice = itemView.findViewById(R.id.tvExperienceListPrice);
            rbExperienceListRating = itemView.findViewById(R.id.rbExperienceListRating);
            tvExperienceListReviews = itemView.findViewById(R.id.tvExperienceListReviews);
            //btnExperienceListEdit = itemView.findViewById(R.id.btnExperienceListEdit);
            //btnExperienceListDelete = itemView.findViewById(R.id.btnExperienceListDelete);
        }
    }
}
