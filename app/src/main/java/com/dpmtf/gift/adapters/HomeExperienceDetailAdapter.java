package com.dpmtf.gift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dpmtf.gift.R;

import java.util.List;

public class HomeExperienceDetailAdapter extends RecyclerView.Adapter<HomeExperienceDetailAdapter.HomeExperienceDetailViewHolder> {

    private final Context context;
    private final List<String> list;

    public HomeExperienceDetailAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public HomeExperienceDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.experience_detail_list_row, parent, false);
        return new HomeExperienceDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeExperienceDetailViewHolder holder, int position) {
        String text = list.get(position);
        holder.tvExperienceDetailText.setText(text);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HomeExperienceDetailViewHolder extends RecyclerView.ViewHolder {

        TextView tvExperienceDetailText;

        public HomeExperienceDetailViewHolder(@NonNull View itemView) {
            super(itemView);
            tvExperienceDetailText = itemView.findViewById(R.id.tvExperienceDetailText);
        }
    }
}
