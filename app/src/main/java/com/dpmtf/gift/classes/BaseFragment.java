package com.dpmtf.gift.classes;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;

import com.dpmtf.gift.R;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.utils.Constants;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class BaseFragment extends Fragment {

    public View view;

    public FirebaseAuth mAuth;
    public FirebaseUser currentUser;

    public FirebaseDatabase firebaseDatabase;
    public DatabaseReference databaseReference;

    public FirebaseStorage firebaseStorage;
    public StorageReference storageReference;

    public ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getActivity());
    }

    public void goToFragment(int id) {
        Constants.goToFragment(view, id);
    }

    public void goToFragment(NavDirections action) {
        Constants.goToFragment(view, action);
    }

    public void goToActivity(Class<?> to) {
        assert getActivity() != null;
        Constants.goToActivity((AppCompatActivity)getActivity(), to);
    }

    public void goToActivityAndFinish(Class<?> to) {
        assert getActivity() != null;
        Constants.goToActivityAndFinis((AppCompatActivity)getActivity(), to);
    }

    public void goToActivityAndDestroy(Class<?> to) {
        assert getActivity() != null;
        Constants.goToActivityAndDestroy((AppCompatActivity)getActivity(), to);
    }

    public void initDatabase() {
        mAuth = FirebaseAuth.getInstance();

        assert getActivity() != null;
        FirebaseApp.initializeApp(getActivity());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
    }

    public void showMessage(String message) {
        assert getActivity() != null;
        Constants.showMessage((AppCompatActivity)getActivity(), message);
    }

    public void openPDLoader() {
        Constants.openProgressDialog(progressDialog, getString(R.string.loading_text));
    }

    public void log(String tag, String text) {
        Log.d(tag, text);
    }

    public void finish(Fragment fragment) {
        assert getActivity() != null;
        Constants.removeFragment((AppCompatActivity)getActivity(), fragment);
    }

    public String getExtension(Uri uri) {
        assert getActivity() != null;
        ContentResolver contentResolver = getActivity().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    public SharedPreferences getCredentialsPreferences() {
        assert getActivity() != null;
        return Constants.getCredentialsPreferences(getActivity());
    }

    public SharedPreferences getSessionPreferences() {
        assert getActivity() != null;
        return Constants.getSessionPreferences(getActivity());
    }

    public void saveUserCredentials(String email, String password) {
        assert getActivity() != null;
        Constants.saveUserCredentials(getActivity(), email, password);
    }

    public void saveUserSession(User user) {
        assert getActivity() != null;
        Constants.saveUserSession(getActivity(), user);
    }

    public boolean isUserAdmin() {
        assert getActivity() != null;
        return Constants.isUserAdmin(getActivity());
    }

    public User getCurrentUser() {
        assert getActivity() != null;
        return Constants.getCurrentUser(getActivity());
    }

    public void removeUserSession() {
        assert getActivity() != null;
        Constants.removeUserSession(getActivity());
    }

    public void removeUserCredentials() {
        assert getActivity() != null;
        Constants.removeUserCredentials(getActivity());
    }

    public void logout() {
        mAuth.signOut();
        assert getActivity() != null;
        Constants.logout(getActivity());
    }
}
