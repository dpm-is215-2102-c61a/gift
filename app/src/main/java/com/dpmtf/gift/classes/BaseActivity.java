package com.dpmtf.gift.classes;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.dpmtf.gift.R;
import com.dpmtf.gift.activities.HomeAdminActivity;
import com.dpmtf.gift.activities.HomeUserActivity;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.utils.Constants;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class BaseActivity extends AppCompatActivity {
    public FirebaseAuth mAuth;
    public FirebaseUser currentUser;

    public FirebaseDatabase firebaseDatabase;
    public DatabaseReference databaseReference;

    public FirebaseStorage firebaseStorage;
    public StorageReference storageReference;

    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
    }

    public void initDatabase() {
        mAuth = FirebaseAuth.getInstance();

        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
    }

    public void goToActivity(Class<?> to)
    {
        Constants.goToActivity(this, to);
    }

    public void goToActivityAndFinish(Class<?> to)
    {
        Constants.goToActivityAndFinis(this, to);
    }

    public void goToActivityAndDestroy(Class<?> to) {
        Constants.goToActivityAndDestroy(this, to);
    }

    public void showMessage(String message) {
        Constants.showMessage(this, message);
    }

    public void openPDLoader() {
        Constants.openProgressDialog(progressDialog, getString(R.string.loading_login_text));
    }

    public void log(String tag, String text) {
        Log.d(tag, text);
    }

    public void checkCurrentUser() {
        currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            if (isUserAdmin()) {
                goToActivityAndFinish(HomeAdminActivity.class);
            } else {
                goToActivityAndFinish(HomeUserActivity.class);
            }
        }
    }

    public SharedPreferences getCredentialsPreferences() {
        return Constants.getCredentialsPreferences(this);
    }

    public SharedPreferences getSessionPreferences() {
        return Constants.getSessionPreferences(this);
    }

    public void saveUserCredentials(String email, String password) {
        Constants.saveUserCredentials(this, email, password);
    }

    public void saveUserSession(User user) {
        Constants.saveUserSession(this, user);
    }

    public boolean isUserAdmin() {
        return Constants.isUserAdmin(this);
    }

    public User getCurrentUser() {
        return Constants.getCurrentUser(this);
    }

    public void removeUserSession() {
        Constants.removeUserSession(this);
    }

    public void removeUserCredentials() {
        Constants.removeUserCredentials(this);
    }

    public void logout() {
        mAuth.signOut();
        Constants.logout(this);
    }
}
