package com.dpmtf.gift.classes;

import android.content.Context;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DAOBase {
    private final Context context;

    public FirebaseDatabase firebaseDatabase;
    public DatabaseReference databaseReference;

    public DAOBase(Context context) {
        this.context = context;
        initDatabase();
    }

    public void initDatabase() {
        FirebaseApp.initializeApp(context);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }
}
