package com.dpmtf.gift.services;

import com.dpmtf.gift.entities.Category;
import com.dpmtf.gift.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CategoryService {

    @GET(Constants.DB_TABLE_CATEGORIES)
    Call<List<Category>> list();

    @GET(Constants.DB_TABLE_CATEGORIES + "/{id}")
    Call<Category> get(@Path("id") String id);

    @POST(Constants.DB_TABLE_CATEGORIES)
    Call<Category> save(@Body Category category);

    @PUT(Constants.DB_TABLE_CATEGORIES + "/{id}")
    Call<Category> update(@Path("id") String id, @Body Category category);

    @DELETE(Constants.DB_TABLE_CATEGORIES + "/{id}")
    Call<Boolean> delete(@Path("id") String id);
}
