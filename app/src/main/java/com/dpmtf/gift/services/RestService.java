package com.dpmtf.gift.services;

import com.dpmtf.gift.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestService {
    public static Retrofit getRestService() {
        return new Retrofit.Builder()
                .baseUrl(Constants.REST_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
