package com.dpmtf.gift.services;

import com.dpmtf.gift.entities.Feature;
import com.dpmtf.gift.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FeatureService {

    @GET(Constants.DB_TABLE_FEATURES)
    Call<List<Feature>> list();

    @GET(Constants.DB_TABLE_FEATURES + "/{id}")
    Call<Feature> get(@Path("id") String id);

    @POST(Constants.DB_TABLE_FEATURES)
    Call<Feature> save(@Body Feature feature);

    @PUT(Constants.DB_TABLE_FEATURES + "/{id}")
    Call<Feature> update(@Path("id") String id, @Body Feature feature);

    @DELETE(Constants.DB_TABLE_FEATURES + "/{id}")
    Call<Boolean> delete(@Path("id") String id);
}
