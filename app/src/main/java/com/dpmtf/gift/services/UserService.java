package com.dpmtf.gift.services;

import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @GET(Constants.DB_TABLE_USERS)
    Call<List<User>> list();

    @GET(Constants.DB_TABLE_USERS + "/users")
    Call<List<User>> users();

    @GET(Constants.DB_TABLE_USERS + "/administrators")
    Call<List<User>> administrators();

    @GET(Constants.DB_TABLE_USERS + "/{id}")
    Call<User> get(@Path("id") String id);

    @POST(Constants.DB_TABLE_USERS)
    Call<User> save(@Body User user);

    @PUT(Constants.DB_TABLE_USERS + "/{id}")
    Call<User> update(@Path("id") String id, @Body User user);

    @DELETE(Constants.DB_TABLE_USERS + "/{id}")
    Call<Boolean> delete(@Path("id") String id);
}
