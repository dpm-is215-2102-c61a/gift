package com.dpmtf.gift.services;

import com.dpmtf.gift.utils.Constants;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FavoriteService {

    @POST(Constants.DB_TABLE_EXPERIENCES + "/{id}" + "/favorites" + "/{userid}")
    Call<Boolean> add(@Path("id") String id, @Path("userid") String userid);

    @DELETE(Constants.DB_TABLE_EXPERIENCES + "/{id}" + "/favorites" + "/{userid}")
    Call<Boolean> delete(@Path("id") String id, @Path("userid") String userid);
}
