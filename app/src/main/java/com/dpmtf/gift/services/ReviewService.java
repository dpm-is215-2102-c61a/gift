package com.dpmtf.gift.services;

import com.dpmtf.gift.entities.Review;
import com.dpmtf.gift.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ReviewService {

    @GET(Constants.DB_TABLE_EXPERIENCES + "/{id}" + "/reviews")
    Call<List<Review>> get(@Path("id") String id);

    @POST(Constants.DB_TABLE_EXPERIENCES + "/{id}" + "/reviews" + "/{userid}")
    Call<List<Review>> add(@Path("id") String id, @Path("userid") String userid);
}
