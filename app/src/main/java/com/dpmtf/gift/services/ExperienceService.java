package com.dpmtf.gift.services;

import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ExperienceService {

    @GET(Constants.DB_TABLE_EXPERIENCES)
    Call<List<Experience>> list();

    @GET(Constants.DB_TABLE_EXPERIENCES + "/popular")
    Call<List<Experience>> popular();

    @GET(Constants.DB_TABLE_EXPERIENCES + "/promotion")
    Call<List<Experience>> promotion();

    @GET(Constants.DB_TABLE_EXPERIENCES + "/{id}" + "/favorites")
    Call<List<Experience>> favorites(@Path("id") String id);

    @GET(Constants.DB_TABLE_CATEGORIES + "/{id}" + "/experiences")
    Call<List<Experience>> byCategory(@Path("id") String id);

    @GET(Constants.DB_TABLE_EXPERIENCES + "/{id}")
    Call<Experience> get(@Path("id") String id);

    @POST(Constants.DB_TABLE_EXPERIENCES)
    Call<Experience> save(@Body Experience experience);

    @PUT(Constants.DB_TABLE_EXPERIENCES + "/{id}")
    Call<Experience> update(@Path("id") String id, @Body Experience experience);

    @DELETE(Constants.DB_TABLE_EXPERIENCES + "/{id}")
    Call<Boolean> delete(@Path("id") String id);
}
