package com.dpmtf.gift.utils;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.dpmtf.gift.R;
import com.dpmtf.gift.entities.Experience;
import com.dpmtf.gift.entities.Location;
import com.dpmtf.gift.entities.User;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final String DB_TABLE_USERS = "user";
    public static final String DB_TABLE_CATEGORIES = "category";
    public static final String DB_TABLE_EXPERIENCES = "experience";
    public static final String DB_TABLE_FEATURES = "feature";

    public static final String REST_BASE_URL = "https://gift.dpm-is215-2102-c61a.ml/";

    public static final String DEFAULT_PASSWORD = "123456";
    public static final String IMAGE_DEFAULT_PROFILE = "https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/profile_img.png?alt=media&token=e322c288-30b9-46d7-82f5-19251d67b8d6";
    public static final String IMAGE_DEFAULT_FEATURE = "https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/feature_default.png?alt=media&token=5b55339a-9560-4f7b-bcd9-29c313ecf21c";
    public static final String IMAGE_DEFAULT_CATEGORY = "https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/category%2FF13gxBEXJYS2L0DFUnghX6DKT7s1.jpg?alt=media&token=4f080467-6a39-4760-82ee-7792fc09ae36";

    public static void goToFragment(View view, int fragment) {
        Navigation.findNavController(view).navigate(fragment);
    }

    public static void goToFragment(View view, NavDirections action) {
        Navigation.findNavController(view).navigate(action);
    }

    public static void goToActivity(AppCompatActivity activity, Class<?> to) {
        Intent intent = new Intent(activity, to);
        activity.startActivity(intent);
    }

    public static void goToActivityAndFinis(AppCompatActivity activity, Class<?> to) {
        goToActivity(activity, to);
        activity.finish();
    }

    public static void goToActivityAndDestroy(AppCompatActivity activity, Class<?> to) {
        Intent intent = new Intent(activity, to);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void removeFragment(AppCompatActivity activity, Fragment fragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(fragment);
        fragmentTransaction.commit();
    }

    public static void showMessage(AppCompatActivity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public static void openProgressDialog(ProgressDialog progressDialog, String message) {
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static String getExtension(Uri uri, ContentResolver contentResolver) {
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    public static SharedPreferences getCredentialsPreferences(Context context) {
        return context.getSharedPreferences("credentials_data", Context.MODE_PRIVATE);
    }

    public static SharedPreferences getSessionPreferences(Context context) {
        return context.getSharedPreferences("login_data", Context.MODE_PRIVATE);
    }

    public static void saveUserCredentials(Context context, String email, String password) {
        SharedPreferences preferences = getCredentialsPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("email", email);
        editor.putString("password", password);
        editor.apply();
    }

    public static void saveUserSession(Context context, User user) {
        SharedPreferences preferences = getSessionPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("id", user.getId());
        editor.putString("name", user.getName());
        editor.putString("lastname", user.getLastname());
        editor.putString("email", user.getEmail());
        editor.putString("image", user.getImage());
        editor.putBoolean("is_admin", user.isAdmin());
        editor.apply();
    }

    public static boolean isUserAdmin(Context context) {
        return getCurrentUser(context).isAdmin();
    }

    public static User getCurrentUser(Context context) {
        SharedPreferences preferences = getSessionPreferences(context);
        String id = preferences.getString("id", "");
        String name = preferences.getString("name", "");
        String lastname = preferences.getString("lastname", "");
        String email = preferences.getString("email", "");
        String image = preferences.getString("image", "");
        boolean isAdmin = preferences.getBoolean("is_admin", false);

        return new User(id, name, lastname, email, image, isAdmin);
    }

    public static void removeUserSession(Context context) {
        SharedPreferences preferences = getSessionPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void removeUserCredentials(Context context) {
        SharedPreferences preferences = getCredentialsPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void logout(Context context) {
        removeUserSession(context);
        removeUserCredentials(context);
    }

    public static List<String> getExperiencesIds(List<Experience> experiences) {
        List<String> ids = new ArrayList<String>();
        for (Experience experience: experiences) {
            ids.add(experience.getId());
        }
        return ids;
    }

    public static List<String> detailtToList(String detail) {
        return Arrays.asList(detail.split("\n"));
    }

    public static LatLng getMarker(double latitude, double longitude) {
        return new LatLng(latitude, longitude);
    }

    public static LatLng getMarker(Location location) {
        double latitude = Double.parseDouble(location.getLatitude());
        double longitude = Double.parseDouble(location.getLongitude());
        return getMarker(latitude, longitude);
    }

    public static MarkerOptions getMarkerOptions(Context context, Location location) {
        LatLng marker = getMarker(location);
        BitmapDescriptor icon = changeIcon(context, R.drawable.location);
        return new MarkerOptions().position(marker).title(location.getName()).icon(icon);
    }

    public static MarkerOptions getMarkerOptions(LatLng marker) {
        return new MarkerOptions().position(marker);
    }

    public static BitmapDescriptor changeIcon(Context context, int id) {
        Drawable icon = ContextCompat.getDrawable(context, id);
        assert icon != null;
        icon.setBounds(0, 0, icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        icon.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
