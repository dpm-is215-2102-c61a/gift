package com.dpmtf.gift.utils;

public enum ExperienceOrigin {
    FAVORITE,
    CATEGORY
}
