package com.dpmtf.gift.utils;

public enum Experiences {
    ALL,
    BY_CATEGORY,
    POPULAR,
    PROMOTION,
}
