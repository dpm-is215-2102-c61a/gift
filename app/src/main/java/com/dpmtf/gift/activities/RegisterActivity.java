package com.dpmtf.gift.activities;

import androidx.annotation.NonNull;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dpmtf.gift.R;
import com.dpmtf.gift.classes.BaseActivity;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.services.UserService;
import com.dpmtf.gift.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity {

    private EditText etRegisterName, etRegisterLastname, etRegisterEmail, etRegisterPassword, etRegisterPasswordConfirm;
    private Button btnRegister;

    UserService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setTitle(getString(R.string.register));

        initDatabase();

        setup();
        checkCurrentUser();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!valid()) {
                    return;
                }

                Constants.openProgressDialog(progressDialog, getString(R.string.loading_register_text));

                String name = etRegisterName.getText().toString();
                String lastname = etRegisterLastname.getText().toString();
                String email = etRegisterEmail.getText().toString();
                String password = etRegisterPassword.getText().toString();

                User user = new User();
                user.setName(name);
                user.setLastname(lastname);
                user.setEmail(email);
                user.setImage(Constants.IMAGE_DEFAULT_PROFILE);
                user.setAdmin(false);

                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    currentUser = mAuth.getCurrentUser();
                                    assert currentUser != null;
                                    user.setId(currentUser.getUid());
                                    createUser(user, password);
                                } else {
                                    showMessage(getString(R.string.message_error_register));
                                    progressDialog.dismiss();
                                }
                            }
                        })
                        .addOnFailureListener(RegisterActivity.this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                showMessage(getString(R.string.message_error_register));
                                progressDialog.dismiss();
                            }
                        });
            }
        });
    }

    protected boolean valid() {
        String name = etRegisterName.getText().toString();
        String lastname = etRegisterLastname.getText().toString();
        String email = etRegisterEmail.getText().toString();
        String password = etRegisterPassword.getText().toString();
        String passwordConfirm = etRegisterPasswordConfirm.getText().toString();

        if (name.equals("")) {
            etRegisterName.setError(getString(R.string.message_error_name));
            etRegisterName.setFocusable(true);
            return false;
        }

        if (lastname.equals("")) {
            etRegisterLastname.setError(getString(R.string.message_error_lastname));
            etRegisterLastname.setFocusable(true);
            return false;
        }

        if (email.equals("")) {
            etRegisterEmail.setError(getString(R.string.message_error_email));
            etRegisterEmail.setFocusable(true);
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etRegisterEmail.setError(getString(R.string.message_error_invalid_email));
            etRegisterEmail.setFocusable(true);
            return false;
        }

        if (password.equals("")) {
            etRegisterPassword.setError(getString(R.string.message_error_password));
            etRegisterPassword.setFocusable(true);
            return false;
        }

        if (password.length() < 6) {
            etRegisterPassword.setError(getString(R.string.message_error_password_length));
            etRegisterPassword.setFocusable(true);
            return false;
        }

        if (passwordConfirm.equals("")) {
            etRegisterPasswordConfirm.setError(getString(R.string.message_error_password_confirm));
            etRegisterPasswordConfirm.setFocusable(true);
            return false;
        }

        if (!password.equals(passwordConfirm)) {
            etRegisterPasswordConfirm.setError(getString(R.string.message_error_passwords));
            etRegisterPasswordConfirm.setFocusable(true);
            return false;
        }

        return true;
    }

    protected void setup() {
        etRegisterName = findViewById(R.id.etRegisterName);
        etRegisterLastname = findViewById(R.id.etRegisterLastname);
        etRegisterEmail = findViewById(R.id.etRegisterEmail);
        etRegisterPassword = findViewById(R.id.etRegisterPassword);
        etRegisterPasswordConfirm = findViewById(R.id.etRegisterPasswordConfirm);
        btnRegister = findViewById(R.id.btnRegister);

        service = RestService.getRestService().create(UserService.class);
    }

    protected void createUser(User user, String password) {
        Call<User> call = service.save(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                saveUserSession(user);
                saveUserCredentials(user.getEmail(), password);
                progressDialog.dismiss();
                if (isUserAdmin()) {
                    goToActivityAndFinish(HomeAdminActivity.class);
                } else {
                    goToActivityAndFinish(HomeUserActivity.class);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                progressDialog.dismiss();
                showMessage(getString(R.string.error));
            }
        });
    }
}
