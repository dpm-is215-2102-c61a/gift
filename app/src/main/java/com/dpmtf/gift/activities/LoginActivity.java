package com.dpmtf.gift.activities;

import androidx.annotation.NonNull;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dpmtf.gift.R;
import com.dpmtf.gift.classes.BaseActivity;
import com.dpmtf.gift.entities.User;
import com.dpmtf.gift.services.RestService;
import com.dpmtf.gift.services.UserService;
import com.dpmtf.gift.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private EditText etLoginEmail, etLoginPassword;
    private Button btnLogin;

    UserService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setTitle(getString(R.string.login));

        initDatabase();

        setup();
        checkCurrentUser();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!valid()) {
                    return;
                }

                Constants.openProgressDialog(progressDialog, getString(R.string.loading_login_text));

                String email = etLoginEmail.getText().toString();
                String password = etLoginPassword.getText().toString();

                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    currentUser = mAuth.getCurrentUser();
                                    assert currentUser != null;
                                    loadUser(currentUser.getUid(), password);
                                } else {
                                    showMessage(getString(R.string.message_error_login));
                                }
                            }
                        })
                        .addOnFailureListener(LoginActivity.this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                showMessage(getString(R.string.message_error_login));
                                progressDialog.dismiss();
                            }
                        });
            }
        });
    }

    protected boolean valid() {
        String email = etLoginEmail.getText().toString();
        String password = etLoginPassword.getText().toString();

        if (email.equals("")) {
            etLoginEmail.setError(getString(R.string.message_error_email));
            etLoginEmail.setFocusable(true);
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etLoginEmail.setError(getString(R.string.message_error_invalid_email));
            etLoginEmail.setFocusable(true);
            return false;
        }

        if (password.equals("")) {
            etLoginPassword.setError(getString(R.string.message_error_password));
            etLoginPassword.setFocusable(true);
            return false;
        }

        if (password.length() < 6) {
            etLoginPassword.setError(getString(R.string.message_error_password_length));
            etLoginPassword.setFocusable(true);
            return false;
        }

        return true;
    }

    protected void setup() {
        etLoginEmail = findViewById(R.id.etLoginEmail);
        etLoginPassword = findViewById(R.id.etLoginPassword);
        btnLogin = findViewById(R.id.btnLogin);

        service = RestService.getRestService().create(UserService.class);
    }

    protected void loadUser(String id, String password) {
        Call<User> call = service.get(id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                assert response.body() != null;
                User user = response.body();
                saveUserSession(user);
                saveUserCredentials(user.getEmail(), password);
                progressDialog.dismiss();
                if (isUserAdmin()) {
                    goToActivityAndFinish(HomeAdminActivity.class);
                } else {
                    goToActivityAndFinish(HomeUserActivity.class);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                progressDialog.dismiss();
                logout();
                showMessage(getString(R.string.error));
            }
        });
    }
}
