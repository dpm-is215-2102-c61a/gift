package com.dpmtf.gift.activities;

import android.os.Bundle;
import android.os.Handler;

import com.dpmtf.gift.R;
import com.dpmtf.gift.classes.BaseActivity;
import com.dpmtf.gift.utils.Constants;

public class LogoutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);

        initDatabase();
        logout();
        Constants.openProgressDialog(progressDialog, getString(R.string.loading_logout_text));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                goToActivityAndDestroy(MainActivity.class);

            }
        }, 1000);
    }
}
