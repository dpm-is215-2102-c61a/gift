package com.dpmtf.gift.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.classes.BaseActivity;
import com.dpmtf.gift.databinding.ActivityHomeAdminBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

public class HomeAdminActivity extends BaseActivity {

    private static final float END_SCALE = 0.85f;

    ActivityHomeAdminBinding binding;
    AppBarConfiguration appBarConfiguration;

    TextView tvAdminHomeName;
    TextView tvAdminHomeEmail;

    DrawerLayout drawerLayout;
    BottomNavigationView bottomNavView;
    NavigationView navigationView;
    CoordinatorLayout contentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityHomeAdminBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        drawerLayout = binding.drawerLayout;
        bottomNavView = binding.appBarAdmin.navBottomView;
        navigationView = binding.navDrawerView;
        contentView = binding.appBarAdmin.contentView;

        setSupportActionBar(binding.appBarAdmin.toolbar);
        appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_category_list, R.id.nav_feature_list, R.id.nav_experience_list, R.id.nav_user_list, R.id.nav_administrator_list, R.id.nav_favorite, R.id.nav_cart, R.id.nav_profile)
                .setOpenableLayout(drawerLayout)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_admin);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupWithNavController(bottomNavView, navController);

        drawerLayout.addDrawerListener(animateNavigationDrawerListener());
        navController.addOnDestinationChangedListener(navControllerDestinationChangedListener());

        initDatabase();
        setup();
        checkIfUserIsLogged();
    }

    protected NavController.OnDestinationChangedListener navControllerDestinationChangedListener() {
        return new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                int id = destination.getId();
                bottomNavView.setVisibility(View.INVISIBLE);
                if (R.id.nav_home == id || R.id.nav_home_categories == id || R.id.nav_home_experiences == id || R.id.nav_home_experience == id || R.id.nav_favorite == id || R.id.nav_cart == id || R.id.nav_profile == id) {
                    bottomNavView.setVisibility(View.VISIBLE);
                }
            }
        };
    }

    protected DrawerLayout.SimpleDrawerListener animateNavigationDrawerListener() {
        return new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                final float offsetScale = 1 - diffScaledOffset;
                contentView.setScaleX(offsetScale);
                contentView.setScaleY(offsetScale);

                final float xOffset = drawerView.getWidth() * slideOffset;
                final float xOffsetDiff = contentView.getWidth() * diffScaledOffset / 2;
                final float xTranslation = xOffset - xOffsetDiff;
                contentView.setTranslationX(xTranslation);
            }
        };
    }

    protected void setup() {
        View headerView = navigationView.getHeaderView(0);
        tvAdminHomeName = headerView.findViewById(R.id.tvAdminHomeName);
        tvAdminHomeEmail = headerView.findViewById(R.id.tvAdminHomeEmail);
    }

    protected void checkIfUserIsLogged() {
        currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            goToActivityAndFinish(MainActivity.class);
        } else {
            tvAdminHomeName.setText(getCurrentUser().getName());
            tvAdminHomeEmail.setText(getCurrentUser().getEmail());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_admin);
        return NavigationUI.navigateUp(navController, appBarConfiguration) || super.onSupportNavigateUp();
    }
}
