package com.dpmtf.gift.activities;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.widget.TextView;

import com.dpmtf.gift.R;
import com.dpmtf.gift.classes.BaseActivity;
import com.dpmtf.gift.databinding.ActivityHomeUserBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeUserActivity extends BaseActivity {

    ActivityHomeUserBinding binding;
    AppBarConfiguration appBarConfiguration;

    BottomNavigationView bottomNavView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeUserBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        bottomNavView = binding.navBottomView;
        appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_favorite, R.id.nav_cart, R.id.nav_profile)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main_user);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navBottomView, navController);

        initDatabase();
        checkIfUserIsLogged();
    }

    protected void checkIfUserIsLogged() {
        currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            goToActivityAndFinish(MainActivity.class);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main_user);
        return NavigationUI.navigateUp(navController, appBarConfiguration) || super.onSupportNavigateUp();
    }
}
